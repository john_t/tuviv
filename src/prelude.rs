//! Traits which you probably want to include.

use std::{
    borrow::Cow,
    cell::Cell,
    ops::{RangeBounds, RangeFull, RangeInclusive},
    rc::Rc,
};

use le::{align::Alignment, layout::Rect};

use crate::widgets::{
    cache::CachedBuffers, flexbox, grid, Align, BoxSizing, Cache, Filler,
    LayoutRecord, Ratio, RefWidget, Scroll, SizeConstraint, Stack,
};
pub use crate::Backend;
use crate::{Color, Modifier, Style, StyledText, Widget};

/// Convinience functions based on a builder-pattern
pub trait WidgetExt<'a>: Widget + Sized + 'a {
    /// Aligns the widget on the X axis
    fn align_x(self, alignment: Alignment) -> Align<Self> {
        let mut align = Align::new(self);
        align.align_y = Alignment::Expand;
        align.align_x = alignment;
        align
    }

    /// Aligns the widget on the Y axis
    fn align_y(self, alignment: Alignment) -> Align<Self> {
        let mut align = Align::new(self);
        align.align_x = Alignment::Expand;
        align.align_y = alignment;
        align
    }

    /// Centers the widget
    fn centered(self) -> Align<Self> {
        let mut align = Align::new(self);
        align.align_x = Alignment::Center;
        align.align_y = Alignment::Center;
        align
    }

    /// Makes the widget a fixed size
    fn fixed_size(
        self,
        width: usize,
        height: usize,
    ) -> SizeConstraint<RangeInclusive<usize>, RangeInclusive<usize>, Self>
    {
        SizeConstraint::new(self, width..=width, height..=height)
    }

    /// Makes the widget a fixed width
    fn fixed_width(
        self,
        width: usize,
    ) -> SizeConstraint<RangeInclusive<usize>, RangeFull, Self> {
        SizeConstraint::new(self, width..=width, ..)
    }

    /// Makes the widget a fixed height
    fn fixed_height(
        self,
        height: usize,
    ) -> SizeConstraint<RangeFull, RangeInclusive<usize>, Self> {
        SizeConstraint::new(self, .., height..=height)
    }

    /// Gives with widget size constraints
    fn constrained_size<H, W>(
        self,
        width: W,
        height: H,
    ) -> SizeConstraint<W, H, Self>
    where
        H: RangeBounds<usize>,
        W: RangeBounds<usize>,
    {
        SizeConstraint::new(self, width, height)
    }

    /// Make the widget a [`Flexbox`](crate::widgets::Flexbox) child
    fn to_flex_child(self) -> flexbox::Child<'a> {
        self.into()
    }

    /// Make the widget a [`Grid`](crate::widgets::Grid) child
    fn to_grid_child(self) -> grid::Child<'a> {
        self.into()
    }

    /// Wraps the widget in a [`BoxSizing`]
    fn to_box_sizing(self) -> BoxSizing<Self> {
        self.into()
    }

    /// Wraps the widget in a [`Scroll`]
    fn to_scroll(self) -> Scroll<Self> {
        Scroll::new(self)
    }

    /// Wraps the widget in a [`Ratio`]
    ///
    /// The ratio is x ÷ y
    fn to_ratio(self, ratio: f64) -> Ratio<Self> {
        Ratio::new(self, ratio)
    }

    /// Wraps the widget in a [`LayoutRecord`]
    fn to_layout_record(self, record: Rc<Cell<Rect>>) -> LayoutRecord<Self> {
        LayoutRecord::new(self, record)
    }

    /// Wraps the widget in a [`LayoutRecord`] with the `mapping` field
    fn to_mapped_layout_record<F: Fn(Rect) -> Rect>(
        self,
        record: Rc<Cell<Rect>>,
        mapping: F,
    ) -> LayoutRecord<Self, F> {
        LayoutRecord::with_mapping(self, record, mapping)
    }

    /// Wraps the widget in a [`Cache`]
    fn cached(self, buffers: &CachedBuffers) -> Cache<Self> {
        Cache::new(self, buffers)
    }

    /// Backs the widget with a background [`Stack`]
    fn stacked_background<I: Into<Option<Color>>>(self, color: I) -> Stack<'a> {
        let color = color.into();
        if let Some(color) = color {
            Stack::new().child(Filler::new(" ".bg(color))).child(self)
        } else {
            Stack::new().child(Filler::new(" ".styled())).child(self)
        }
    }
}

/// A trait for static widgets
pub trait WidgetStaticExt: Widget + Sized {
    /// Wraps this in a [`RefWidget`]
    fn to_ref(self) -> RefWidget<Self> {
        self.into()
    }
}

impl<'a, T> WidgetExt<'a> for T where T: Widget + 'a + Sized {}

impl<T> WidgetStaticExt for T where T: Widget + 'static {}

impl<'a, T> From<T> for flexbox::Child<'a>
where
    T: Widget + 'a,
{
    fn from(them: T) -> Self {
        flexbox::Child::new(Box::new(them))
    }
}

impl<T: Widget> From<T> for BoxSizing<T> {
    fn from(them: T) -> Self {
        Self::new(them, crate::border::Border::BLANK)
    }
}

impl<T: Widget> From<T> for Scroll<T> {
    fn from(them: T) -> Self {
        Self::new(them)
    }
}

impl<T> From<T> for RefWidget<T>
where
    T: Widget + 'static,
{
    fn from(them: T) -> Self {
        Self::new(them)
    }
}

impl<'a, T> From<T> for grid::Child<'a>
where
    T: Widget + 'a,
{
    fn from(them: T) -> grid::Child<'a> {
        grid::Child::new(
            Box::new(them),
            grid::Props {
                column: 0,
                row: 0,
                column_span: 1,
                row_span: 1,
            },
        )
    }
}

/// A helper for anything that can be styled
pub trait StylableExt<'a> {
    /// Creates a [`StyledText`] out of this
    fn styled(self) -> StyledText<'a>;
    /// Gives this a specific style
    fn with_style(self, style: Style) -> StyledText<'a>;
    /// Gives this a specific foreground colour
    fn fg(self, fg: impl Into<Option<Color>>) -> StyledText<'a>;
    /// Gives this a specific background colour
    fn bg(self, bg: impl Into<Option<Color>>) -> StyledText<'a>;
    /// Gives this a modifier
    fn with_modifier(self, modifier: Modifier) -> StyledText<'a>;
    /// Removes a modifier from this
    fn without_modifier(self, modifier: Modifier) -> StyledText<'a>;
}

impl<'a, T> StylableExt<'a> for T
where
    Cow<'a, str>: From<T>,
{
    fn styled(self) -> StyledText<'a> {
        StyledText::from(self)
    }
    fn fg(self, fg: impl Into<Option<Color>>) -> StyledText<'a> {
        self.styled().fg(fg)
    }
    fn bg(self, bg: impl Into<Option<Color>>) -> StyledText<'a> {
        self.styled().bg(bg)
    }
    fn with_modifier(self, modifier: Modifier) -> StyledText<'a> {
        self.styled().with_modifier(modifier)
    }
    fn without_modifier(self, modifier: Modifier) -> StyledText<'a> {
        self.styled().without_modifier(modifier)
    }
    fn with_style(self, style: Style) -> StyledText<'a> {
        StyledText {
            style,
            text: self.into(),
        }
    }
}
