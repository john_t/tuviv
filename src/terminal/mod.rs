//! Ways to interact with and represent terminals

pub mod style;
pub use style::{Color, Modifier, Style, StyledText};
pub mod buffer;
pub use buffer::{Buffer, Cell};
pub mod backend;
pub use backend::*;
