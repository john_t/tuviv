//! Houses the [`Char`] widget

use le::{layout::Vec2, Layout, MinimumNatural};
use unicode_width::UnicodeWidthChar;

use crate::{buffer::BufferSliceMut, Style, Widget};

/// A widget that renders a single character
pub struct Char {
    /// The character to display
    character: char,
    /// The style on that character
    style: Style,
}

impl Char {
    /// Creates a new character
    pub fn new(character: char) -> Self {
        Self {
            character,
            style: Style::default(),
        }
    }

    /// Sets the `style` property of this
    pub fn styled(mut self, style: Style) -> Self {
        self.style = style;
        self
    }
}

impl Layout for Char {
    fn width_for_height(&self, _height: usize) -> MinimumNatural<usize> {
        MinimumNatural::same(self.character.width().unwrap_or(1))
    }

    fn height_for_width(&self, _width: usize) -> MinimumNatural<usize> {
        MinimumNatural::same(1)
    }

    fn prefered_size(&self) -> MinimumNatural<Vec2> {
        MinimumNatural::same(Vec2::new(self.width_for_height(1).minimum, 1))
    }

    fn prefered_size_of_container(
        &self,
        _container: Vec2,
    ) -> MinimumNatural<Vec2> {
        self.prefered_size()
    }
}

impl Widget for Char {
    fn render(&self, buffer: &mut BufferSliceMut<'_>) {
        let pos = Vec2::new(0, 0);
        buffer.write_char(self.character, pos);
        buffer.style_cell(self.style, pos);
    }
}
