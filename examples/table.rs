use std::io::{self};
use std::time::Duration;

use le::grid::Sizing;
use tuviv::Style;
use tuviv::{
    border::Border,
    prelude::*,
    widgets::{Grid, Paragraph},
};

fn main() -> io::Result<()> {
    let grid = Grid::new()
        .template_rows(vec![Sizing::Auto; 5])
        .template_columns(vec![Sizing::Auto; 4])
        .row_gap(1)
        .column_gap(1)
        .auto_child(Paragraph::label("Name".styled()))
        .auto_child(Paragraph::label("Designed By".styled()).wrap(false))
        .auto_child(Paragraph::label("Invented Year".styled()).wrap(false))
        .auto_child(Paragraph::label("Rating".styled()).wrap(false))
        .auto_child(Paragraph::label("C".styled()).wrap(false))
        .auto_child(Paragraph::label("Dennis Ritchie".styled()).wrap(false))
        .auto_child(Paragraph::label("1972".styled()).wrap(false))
        .auto_child(Paragraph::label("* * * *".styled()).wrap(false))
        .auto_child(Paragraph::label("Rust".styled()).wrap(false))
        .auto_child(Paragraph::label("Graydon Hoare".styled()).wrap(false))
        .auto_child(Paragraph::label("2010".styled()).wrap(false))
        .auto_child(Paragraph::label("* * * * *".styled()).wrap(false))
        .auto_child(Paragraph::label("Go".styled()).wrap(false))
        .auto_child(Paragraph::label("Rob Pike".styled()).wrap(false))
        .auto_child(Paragraph::label("2009".styled()).wrap(false))
        .auto_child(Paragraph::label("* *".styled()).wrap(false))
        .auto_child(Paragraph::label("Javascript".styled()).wrap(false))
        .auto_child(Paragraph::label("Brendan Eich".styled()).wrap(false))
        .auto_child(Paragraph::label("1995".styled()).wrap(false))
        .auto_child(Paragraph::label("* *".styled()).wrap(false))
        .border(Border::ROUNDED.styled(Style::default().green()))
        .centered();

    tuviv::render_frame(&grid, Duration::from_secs(2))?;

    Ok(())
}
