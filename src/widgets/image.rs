//! Houses the [`Image`] widget

use le::{layout::Vec2, Layout, MinimumNatural};

use crate::{buffer::BufferSliceMut, prelude::*, Color, Widget};
use image::{imageops::FilterType, GenericImageView, Pixel, Rgb};
use std::ops::Deref;

/// Represents either a &ImageBuffer or an ImageBuffer
pub enum ImageBufferB<'a, I: GenericImageView> {
    /// An owned image
    Owned(I),
    /// A borrowed image
    Borrowed(&'a I),
}

impl<I: GenericImageView> Deref for ImageBufferB<'_, I> {
    type Target = I;

    fn deref(&self) -> &Self::Target {
        match self {
            Self::Owned(ref x) => x,
            Self::Borrowed(x) => x,
        }
    }
}

impl<I: GenericImageView> From<I> for ImageBufferB<'static, I> {
    fn from(them: I) -> Self {
        Self::Owned(them)
    }
}

impl<'a, I: GenericImageView> From<&'a I> for ImageBufferB<'a, I> {
    fn from(them: &'a I) -> Self {
        Self::Borrowed(them)
    }
}

/// A widget that renders an image.
///
/// Please note this is done entirely via unicode and
/// does not use technologies such as kitty/sixel/ueberzug to
/// do so.
///
/// *requires the `image` dependency*
pub struct Image<'a, I: GenericImageView> {
    /// The image to render
    pub image: ImageBufferB<'a, I>,
    /// How should the image be resized?
    pub resize_filter: Option<FilterType>,
}

impl<'a, I: GenericImageView> Image<'a, I> {
    /// Creates a new [`Image`]
    pub fn new<T: Into<ImageBufferB<'a, I>>>(image: T) -> Self {
        Self {
            image: image.into(),
            resize_filter: None,
        }
    }

    /// Sets the [`resize_filter`] property on this
    pub fn resize_filter(mut self, filter: FilterType) -> Self {
        self.resize_filter = Some(filter);
        self
    }
}

/// A trait for all image widgets
pub trait ImageLayout {
    /// Gets the dimensions of this
    fn dimensions(&self) -> Vec2;
}

impl<I> ImageLayout for Image<'_, I>
where
    I: GenericImageView,
{
    /// Gets the dimensions of this
    fn dimensions(&self) -> Vec2<usize> {
        let (x, y) = self.image.dimensions();
        Vec2::new(x as usize, y as usize)
    }
}

/// Creates the Layout impl for a widget
macro_rules! impl_layout_for_image {
    () => {
        fn width_for_height(&self, height: usize) -> MinimumNatural<usize> {
            let dimensions = self.dimensions();
            MinimumNatural {
                minimum: 1,
                natural: (height * dimensions.x / dimensions.y * 2).max(1),
            }
        }

        fn height_for_width(&self, width: usize) -> MinimumNatural<usize> {
            let dimensions = self.dimensions();
            MinimumNatural {
                minimum: 1,
                natural: (width * dimensions.y / dimensions.x / 2).max(1),
            }
        }

        fn prefered_size(&self) -> MinimumNatural<Vec2> {
            MinimumNatural::same(Vec2::new(self.width_for_height(1).minimum, 1))
            // Vec2::new(usize::MAX, usize::MAX)
        }

        fn prefered_size_of_container(
            &self,
            container: Vec2,
        ) -> MinimumNatural<Vec2> {
            let dimensions = self.dimensions();

            let mut size = Vec2::new(
                (container.y * dimensions.x / dimensions.y) * 2,
                container.y,
            );
            if size.x > container.x {
                size = Vec2::new(
                    container.x,
                    (container.x * dimensions.y / dimensions.x) / 2,
                );
            }
            MinimumNatural::same(size)
        }
    };
}
pub(crate) use impl_layout_for_image;

impl<I> Layout for Image<'_, I>
where
    I: GenericImageView,
{
    impl_layout_for_image!();
}

impl<I, P> Widget for Image<'_, I>
where
    P: Pixel + 'static,
    I: GenericImageView<Pixel = P>,
    u8: From<<Rgb<<P as Pixel>::Subpixel> as Pixel>::Subpixel>,
    Rgb<<P as Pixel>::Subpixel>: Pixel,
{
    fn render(&self, buffer: &mut BufferSliceMut<'_>) {
        let dimensions = self.dimensions();

        let size = self.prefered_size_of_container(buffer.size()).natural;

        // Rescale the image
        if let Some(resize_filter) = self.resize_filter {
            let resized = image::imageops::resize(
                &*self.image,
                size.x as u32,
                2 * size.y as u32,
                resize_filter,
            );
            for x in 0..size.x {
                for y in 0..size.y {
                    let p1 = resized.get_pixel(x as u32, (2 * y) as u32);
                    let p2 = resized.get_pixel(x as u32, (2 * y + 1) as u32);

                    // Write the pixel
                    let rgb1 = Color::from(Pixel::to_rgb(p1));
                    let rgb2 = Color::from(Pixel::to_rgb(p2));

                    buffer
                        .write(Vec2::new(x, y), "▄".styled().fg(rgb2).bg(rgb1))
                }
            }
        } else {
            // Thumbnail the image
            for x in 0..size.x {
                let abs_x = (x * dimensions.x / size.x)
                    .min(dimensions.x.saturating_sub(1));
                for y in 0..size.y {
                    let max_y = dimensions.y.saturating_sub(1);

                    let abs_y1 = (y * dimensions.y / size.y).min(max_y);
                    let abs_y2 = (y * dimensions.y / size.y + 1).min(max_y);

                    let p1 = self.image.get_pixel(abs_x as u32, abs_y1 as u32);
                    let p2 = self.image.get_pixel(abs_x as u32, abs_y2 as u32);

                    // Write the pixel
                    let rgb1 = Color::from(Pixel::to_rgb(&p1));
                    let rgb2 = Color::from(Pixel::to_rgb(&p2));

                    buffer
                        .write(Vec2::new(x, y), "▄".styled().fg(rgb2).bg(rgb1))
                }
            }
        }
    }
}
