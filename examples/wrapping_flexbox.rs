use std::{
    convert::Infallible,
    io::{self},
};

use crossterm::event::{Event, KeyCode};
use le::Orientation;
use tuviv::{
    prelude::*,
    widgets::{Filler, Flexbox},
    Color,
};

fn main() -> io::Result<()> {
    tuviv::run_app(|event| {
        let mut flexbox = Flexbox::new(Orientation::Horizontal, true)
            .column_gap(2)
            .row_gap(1);

        for i in 0..8 {
            let filler =
                Filler::new(" ".bg(Color::from_ansi(31 + i % 5).unwrap()))
                    .fixed_size(16, 8)
                    .to_flex_child();
            flexbox.children.push(filler);
        }

        if let Event::Key(key) = event {
            if let KeyCode::Char('q') = key.code {
                return Ok(None);
            }
        }

        Ok::<_, Infallible>(Some(flexbox))
    })
}
