#![doc=include_str!("../README.md")]
//! - A good place to start is by looking in the [`widgets`] module.
//! - Most apps will start with [`run_app`]
#![doc(
    html_logo_url = "https://gitlab.com/john_t/tuviv/-/raw/master/icon.png",
    html_favicon_url = "https://gitlab.com/john_t/tuviv/-/raw/master/icon.png"
)]
#![deny(missing_docs, clippy::missing_docs_in_private_items)]

pub mod border;
pub mod prelude;
pub mod terminal;
use std::{
    error::Error,
    io::{self, stdout, Write},
};

use buffer::BufferSliceMut;
use crossterm::event::EnableMouseCapture;
#[cfg(feature = "crossterm")]
use crossterm::{
    cursor,
    event::DisableMouseCapture,
    execute,
    terminal::{
        disable_raw_mode, enable_raw_mode, EnterAlternateScreen,
        LeaveAlternateScreen,
    },
};
use std::time::Duration;
pub use terminal::Buffer;
pub use terminal::*;
pub mod widgets;

pub use le;
use le::{
    layout::{Rect, Vec2},
    MinimumNatural,
};

/// A type representing a widget.
type WidgetType<'a> = Box<dyn Widget + 'a>;

/// A base trait for all widgets - it extends from
/// `layout_engines` layout to allow for layout while having
/// a [`render`](Widget::render) function to write to a buffer.
pub trait Widget: le::Layout {
    /// Renders the widget to the writer
    fn render(&self, buffer: &mut BufferSliceMut<'_>);
}

impl le::Layout for Box<dyn Widget + '_> {
    fn width_for_height(&self, height: usize) -> MinimumNatural<usize> {
        (**self).width_for_height(height)
    }

    fn height_for_width(&self, width: usize) -> MinimumNatural<usize> {
        (**self).height_for_width(width)
    }

    fn prefered_size(&self) -> MinimumNatural<Vec2> {
        (**self).prefered_size()
    }

    fn prefered_size_of_container(
        &self,
        container: Vec2,
    ) -> MinimumNatural<Vec2> {
        (**self).prefered_size_of_container(container)
    }
}

impl Widget for Box<dyn Widget + '_> {
    fn render(&self, buffer: &mut BufferSliceMut<'_>) {
        (**self).render(buffer)
    }
}

impl<T> Widget for &T
where
    T: Widget,
{
    fn render(&self, buffer: &mut BufferSliceMut<'_>) {
        T::render(self, buffer)
    }
}

/**
Runs a tuviv app

This sets up all the terminal

- It takes a function (or closure) that:
    - Takes a `crossterm::event::Event` as input
    - Returns:
        - `Ok(Some(W))` on sucess - thus rendering a widget
        - `Ok(None)` for a graceful closing of the application
        - `Err(e)` for an error - which is printed to the stderr by tuviv

- The generics are:
    - `W`: the widget this returns
    - `E`: the error type. Use [`Infallible`](std::convert::Infallible) if `FW` doesn't fail
    - `FW`: the funtion

- A simple example function would be:
 ```rust
run_app(|event| {
    // Allows for the app to be quit
    if let Event::Key(key) = event {
        if let KeyCode::Char('q') = key.code {
            return Ok(None);
        }
    }
    // Create a widget
    let w = Paragraph::label("Press q to to quit").centered();

    // This app never fails, so we return Infallible (`!`)
    Ok::<_, Infallible>(w)
})
```
*/
#[cfg(feature = "crossterm")]
pub fn run_app<
    W: Widget,
    E: Error,
    FW: FnMut(crossterm::event::Event) -> Result<Option<W>, E>,
>(
    mut func: FW,
) -> io::Result<()> {
    use crossterm::event::{self, Event};
    let mut app = App::new()?;

    let mut err = None;

    let mut first_time = true;

    // Run the thing
    loop {
        let res = if first_time {
            let (x, y) = crossterm::terminal::size()?;
            func(Event::Resize(x, y))
        } else {
            func(event::read()?)
        };
        match res {
            Ok(okay) => match okay {
                Some(w) => {
                    app.render_widget(&w)?;
                }
                None => break,
            },
            Err(e) => {
                err = Some(e);
                break;
            }
        }
        first_time = false;
    }

    if let Some(err) = err {
        eprintln!("Error: {}", err);
    }

    Ok(())
}

/// Runs an app on an individual frame for a given Duration
///
/// Mainly useful for testing layouts, see [`run_app`] for
/// a more useful function
///
/// This will setup the terminal
#[cfg(feature = "crossterm")]
pub fn render_frame<W: Widget>(w: &W, duration: Duration) -> io::Result<()> {
    use std::thread;

    let mut app = App::new()?;

    let _ = execute!(stdout(), DisableMouseCapture,);
    app.render_widget(w)?;
    thread::sleep(duration);

    Ok(())
}

/// A struct to run an app
///
/// Use this if `run_app` is too basic for your use case,
/// i.e. if you want to use `async`
#[cfg(feature = "crossterm")]
pub struct App {
    /// The previous buffer
    prev_buffer: Buffer,
}

#[cfg(feature = "crossterm")]
impl App {
    /// Creates a new `App`
    ///
    /// This will set up the crossterm environment.
    /// When this is dropped then it will restore
    /// the terminal environment
    pub fn new() -> io::Result<Self> {
        enable_raw_mode()?;
        execute!(
            stdout(),
            EnterAlternateScreen,
            EnableMouseCapture,
            cursor::Hide
        )?;

        // Ensure images can be rendered in tmux
        if std::env::var("tmux").is_ok() {
            std::process::Command::new("tmux")
                .args(["set", "-p", "allow-passthrough", "on"])
                .spawn()?
                .wait()?;
        }

        Ok(Self {
            prev_buffer: Buffer::new(Vec2::new(0, 0)),
        })
    }

    /// Renders a widget
    pub fn render_widget<W: Widget>(&mut self, w: W) -> io::Result<()> {
        let (x, y) = crossterm::terminal::size()?;
        let x = x as usize;
        let y = y as usize;
        let mut buffer = Buffer::new(Vec2::new(x, y));
        w.render(&mut buffer.as_mut_slice());
        CrosstermBackend.finish(
            &buffer.as_slice(),
            &self.prev_buffer.as_slice(),
            &mut stdout(),
        )?;
        stdout().flush()?;
        self.prev_buffer = buffer;
        Ok(())
    }
}

impl Drop for App {
    fn drop(&mut self) {
        let _ = disable_raw_mode();
        let _ = execute!(
            stdout(),
            LeaveAlternateScreen,
            DisableMouseCapture,
            cursor::Show
        );
    }
}
