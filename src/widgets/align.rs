//! Houses the [`Align`] widget

use le::{layout::LayoutInfo, Alignment, Layout, MinimumNatural};

use crate::{
    buffer::BufferSliceMut,
    le::layout::{Rect, Vec2},
    Widget,
};

/// Aligns the child widget within its space
///
/// `Align` will always pass through layout functions to the child.
/// `Align` will try and align based of prefered size but if it
/// is not available than Align will stretch it as required.
pub struct Align<T: Widget> {
    /// The child widget
    pub widget: T,
    /// How to align on the x axis
    pub align_x: Alignment,
    /// How to align on the y axis
    pub align_y: Alignment,
}

impl<T: Widget> Align<T> {
    /// Creates a new `Align`
    pub fn new(widget: T) -> Self {
        Self {
            widget,
            align_x: Alignment::Center,
            align_y: Alignment::Center,
        }
    }

    /// Sets the [`Align::align_x`] property
    pub fn align_x(mut self, alignment: Alignment) -> Self {
        self.align_x = alignment;
        self
    }

    /// Sets the [`Align::align_y`] property
    pub fn align_y(mut self, alignment: Alignment) -> Self {
        self.align_y = alignment;
        self
    }
}

impl<W: Widget> Layout for Align<W> {
    fn width_for_height(&self, height: usize) -> MinimumNatural<usize> {
        self.widget.width_for_height(height)
    }
    fn height_for_width(&self, width: usize) -> MinimumNatural<usize> {
        self.widget.height_for_width(width)
    }

    fn prefered_size(&self) -> MinimumNatural<Vec2> {
        self.widget.prefered_size()
    }

    fn prefered_size_of_container(
        &self,
        container: Vec2,
    ) -> MinimumNatural<Vec2> {
        self.widget.prefered_size_of_container(container)
    }
}

impl<W: Widget> Widget for Align<W> {
    fn render(&self, buffer: &mut BufferSliceMut<'_>) {
        let buffer_size = buffer.size();
        // Get the widget size
        let mut widget_size =
            self.widget.prefered_size_of_container(buffer_size).natural;
        if self.align_x == Alignment::Expand {
            widget_size.x = buffer_size.x;
        }
        if self.align_y == Alignment::Expand {
            widget_size.y = buffer_size.y;
        }

        if widget_size.y > buffer_size.y {
            widget_size.x = self.widget.width_for_height(buffer_size.y).natural;
            widget_size.y = buffer_size.y;
        }

        if widget_size.x > buffer_size.x {
            widget_size.y = self.widget.height_for_width(buffer_size.x).natural;
            widget_size.x = buffer_size.x;
        }

        // Separate the rect into layout infos
        let li_x = LayoutInfo {
            start: 0,
            end: buffer_size.x,
        };
        let li_y = LayoutInfo {
            start: 0,
            end: buffer_size.y,
        };

        // Align them
        let al_x = self.align_x.align(li_x, widget_size.x);
        let al_y = self.align_y.align(li_y, widget_size.y);

        // Reconstruct it into a rect
        let aligned_rect = Rect::new(
            al_x.start,
            al_y.start,
            al_x.end - al_x.start,
            al_y.end - al_y.start,
        );

        let mut buffer_slice_mut = buffer.sub_mut_slice(aligned_rect).unwrap();
        self.widget.render(&mut buffer_slice_mut);
    }
}
