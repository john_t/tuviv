use std::io::{self};
use std::time::Duration;

use le::grid::Sizing;
use tuviv::{
    prelude::*,
    widgets::{Filler, Grid},
};

fn main() -> io::Result<()> {
    let grid = Grid::new()
        .template_rows(vec![Sizing::Auto; 3])
        .template_columns(vec![Sizing::Auto; 4])
        .row_gap(1)
        .column_gap(2)
        .child(
            Filler::new(" ".styled().bg_red())
                .to_grid_child()
                .row(0)
                .column(0)
                .column_span(2),
        )
        .child(
            Filler::new(" ".styled().bg_yellow())
                .to_grid_child()
                .row(0)
                .column(2)
                .row_span(2)
                .column_span(2),
        )
        .child(
            Filler::new(" ".styled().bg_green())
                .to_grid_child()
                .row(1)
                .column(0)
                .row_span(1)
                .column_span(1),
        )
        .child(
            Filler::new(" ".styled().bg_blue())
                .to_grid_child()
                .row(1)
                .column(1)
                .row_span(1)
                .column_span(1),
        )
        .child(
            Filler::new(" ".styled().bg_magenta())
                .to_grid_child()
                .row(2)
                .column(0)
                .row_span(1)
                .column_span(4),
        );

    tuviv::render_frame(&grid, Duration::from_secs(2))?;

    Ok(())
}
