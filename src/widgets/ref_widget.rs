//! Houses the [`RefWidget`]

use std::cell::RefCell;
use std::rc::Rc;

use le::{Layout, MinimumNatural};

use crate::{buffer::BufferSliceMut, le::layout::Vec2, Widget};

/// A widget which allows one to retain a reference to a widget.
///
/// It is not recommended to use this type
pub struct RefWidget<T: Widget + 'static> {
    /// The widget which is referenced
    pub widget: Rc<RefCell<T>>,
}

impl<T: Widget + 'static> RefWidget<T> {
    /// Creates a new [`RefWidget`]
    pub fn new(widget: T) -> Self {
        Self {
            widget: Rc::new(RefCell::new(widget)),
        }
    }

    /// Creates a [`RefWidget`] with an already existing [`Rc`]
    pub fn new_rc(widget: Rc<RefCell<T>>) -> Self {
        Self { widget }
    }
}

impl<T: Widget + 'static> Layout for RefWidget<T> {
    fn width_for_height(&self, height: usize) -> MinimumNatural<usize> {
        self.widget.borrow().width_for_height(height)
    }

    fn height_for_width(&self, width: usize) -> MinimumNatural<usize> {
        self.widget.borrow().height_for_width(width)
    }

    fn prefered_size(&self) -> MinimumNatural<Vec2> {
        self.widget.borrow().prefered_size()
    }

    fn prefered_size_of_container(
        &self,
        container: Vec2,
    ) -> MinimumNatural<Vec2> {
        self.widget.borrow().prefered_size_of_container(container)
    }
}

impl<T: Widget + 'static> Widget for RefWidget<T> {
    fn render(&self, buffer: &mut BufferSliceMut) {
        self.widget.borrow().render(buffer)
    }
}
