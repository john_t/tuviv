//! Ways of styling the terminal

pub use std::borrow::Cow;
use std::{
    fmt::{self, Display, LowerHex},
    mem,
    str::FromStr,
};

#[cfg(feature = "crossterm")]
use crossterm::style::{Attribute as CrossAttribute, Color as CrossColor};

/// Creates a function to set this to a specific colour
macro_rules! color_func {
    ($funcname: ident, $color: path, $colorfunc: ident) => {
        #[doc = concat!("A shorthand for `self.", stringify!($colorfunc), "(", stringify!($color), ")`")]
        #[must_use = "its a builder pattern function"]
        pub fn $funcname(self) -> Self {
            self.$colorfunc($color)
        }
    }
}

/// Creates a function to set a modifier on this
macro_rules! modifier_func {
    ($funcname: ident, $modifier: path, $attrfunc: ident) => {
        #[doc = concat!("A shorthand for `self.width_modifier(", stringify!($modifier), ")`")]
        #[must_use = "its a builder pattern function"]
        pub fn $funcname(self) -> Self {
            self.$attrfunc($modifier)
        }
    }
}

/// Creates all the [`color_func`]s and [`modifier_func`]s
macro_rules! funcs {
    () => {
        color_func!(black, Color::Black, fg);
        color_func!(red, Color::Red, fg);
        color_func!(green, Color::Green, fg);
        color_func!(yellow, Color::Yellow, fg);
        color_func!(blue, Color::Blue, fg);
        color_func!(magenta, Color::Magenta, fg);
        color_func!(cyan, Color::Cyan, fg);
        color_func!(white, Color::White, fg);

        color_func!(bg_black, Color::Black, bg);
        color_func!(bg_red, Color::Red, bg);
        color_func!(bg_green, Color::Green, bg);
        color_func!(bg_yellow, Color::Yellow, bg);
        color_func!(bg_blue, Color::Blue, bg);
        color_func!(bg_magenta, Color::Magenta, bg);
        color_func!(bg_cyan, Color::Cyan, bg);
        color_func!(bg_white, Color::White, bg);

        color_func!(underline_colored_black, Color::Black, underline_color);
        color_func!(underline_colored_red, Color::Red, underline_color);
        color_func!(underline_colored_green, Color::Green, underline_color);
        color_func!(underline_colored_yellow, Color::Yellow, underline_color);
        color_func!(underline_colored_blue, Color::Blue, underline_color);
        color_func!(underline_colored_magenta, Color::Magenta, underline_color);
        color_func!(underline_colored_cyan, Color::Cyan, underline_color);
        color_func!(underline_colored_white, Color::White, underline_color);

        color_func!(bright_black, Color::BrightBlack, fg);
        color_func!(bright_red, Color::BrightRed, fg);
        color_func!(bright_green, Color::BrightGreen, fg);
        color_func!(bright_yellow, Color::BrightYellow, fg);
        color_func!(bright_blue, Color::BrightBlue, fg);
        color_func!(bright_magenta, Color::BrightMagenta, fg);
        color_func!(bright_cyan, Color::BrightCyan, fg);
        color_func!(bright_white, Color::BrightWhite, fg);

        color_func!(bright_bg_black, Color::BrightBlack, bg);
        color_func!(bright_bg_red, Color::BrightRed, bg);
        color_func!(bright_bg_green, Color::BrightGreen, bg);
        color_func!(bright_bg_yellow, Color::BrightYellow, bg);
        color_func!(bright_bg_blue, Color::BrightBlue, bg);
        color_func!(bright_bg_magenta, Color::BrightMagenta, bg);
        color_func!(bright_bg_cyan, Color::BrightCyan, bg);
        color_func!(bright_bg_white, Color::BrightWhite, bg);

        color_func!(
            underline_colored_bright_black,
            Color::BrightBlack,
            underline_color
        );
        color_func!(
            underline_colored_bright_red,
            Color::BrightRed,
            underline_color
        );
        color_func!(
            underline_colored_bright_green,
            Color::BrightGreen,
            underline_color
        );
        color_func!(
            underline_colored_bright_yellow,
            Color::BrightYellow,
            underline_color
        );
        color_func!(
            underline_colored_bright_blue,
            Color::BrightBlue,
            underline_color
        );
        color_func!(
            underline_colored_bright_magenta,
            Color::BrightMagenta,
            underline_color
        );
        color_func!(
            underline_colored_bright_cyan,
            Color::BrightCyan,
            underline_color
        );
        color_func!(
            underline_colored_bright_white,
            Color::BrightWhite,
            underline_color
        );

        modifier_func!(not_bold, Modifier::BOLD, without_modifier);
        modifier_func!(not_dimmed, Modifier::DIM, without_modifier);
        modifier_func!(not_italic, Modifier::ITALIC, without_modifier);
        modifier_func!(not_underlined, Modifier::UNDERLINED, without_modifier);
        modifier_func!(
            not_double_underlined,
            Modifier::DOUBLE_UNDERLINED,
            without_modifier
        );
        modifier_func!(
            not_undercurled,
            Modifier::UNDERCURLED,
            without_modifier
        );
        modifier_func!(
            not_underdotted,
            Modifier::UNDERDOTTED,
            without_modifier
        );
        modifier_func!(
            not_underfashed,
            Modifier::UNDERDASHED,
            without_modifier
        );
        modifier_func!(not_slow_blink, Modifier::SLOW_BLINK, without_modifier);
        modifier_func!(
            not_rapid_blink,
            Modifier::RAPID_BLINK,
            without_modifier
        );
        modifier_func!(not_reversed, Modifier::REVERSED, without_modifier);
        modifier_func!(not_hidden, Modifier::HIDDEN, without_modifier);
        modifier_func!(
            not_crossed_out,
            Modifier::CROSSED_OUT,
            without_modifier
        );

        modifier_func!(bold, Modifier::BOLD, with_modifier);
        modifier_func!(dimmed, Modifier::DIM, with_modifier);
        modifier_func!(italic, Modifier::ITALIC, with_modifier);
        modifier_func!(underlined, Modifier::UNDERLINED, with_modifier);
        modifier_func!(
            double_underlined,
            Modifier::DOUBLE_UNDERLINED,
            with_modifier
        );
        modifier_func!(undercurled, Modifier::UNDERCURLED, with_modifier);
        modifier_func!(underdotted, Modifier::UNDERDOTTED, with_modifier);
        modifier_func!(underfashed, Modifier::UNDERDASHED, with_modifier);
        modifier_func!(slow_blink, Modifier::SLOW_BLINK, with_modifier);
        modifier_func!(rapid_blink, Modifier::RAPID_BLINK, with_modifier);
        modifier_func!(reversed, Modifier::REVERSED, with_modifier);
        modifier_func!(hidden, Modifier::HIDDEN, with_modifier);
        modifier_func!(crossed_out, Modifier::CROSSED_OUT, with_modifier);
    };
}
/// A style representing all the colours and modifiers a cell can have.
#[derive(Clone, Copy, Debug, Default, PartialEq, Eq)]
#[cfg_attr(feature = "serde", derive(serde::Deserialize, serde::Serialize))]
pub struct Style {
    /// The foreground (text-colour) of the cell
    #[cfg_attr(feature = "serde", serde(default))]
    pub fg: Option<Color>,
    /// The background colour of the cell
    #[cfg_attr(feature = "serde", serde(default))]
    pub bg: Option<Color>,
    /// The underline colour of the cell
    #[cfg_attr(feature = "serde", serde(default))]
    pub underline_color: Option<Color>,
    /// The modifiers this has
    #[cfg_attr(feature = "serde", serde(alias = "modifiers"))]
    #[cfg_attr(feature = "serde", serde(default))]
    pub add_modifier: Modifier,
    /// The modifiers this specifically doesn't
    ///
    /// This is mainly used when overlaying [`Style`]s.
    #[cfg_attr(feature = "serde", serde(default))]
    pub sub_modifier: Modifier,
}

impl Style {
    /// Overlays another style onto this.
    #[must_use = "this returns a new modifier"]
    pub fn overlay(self, overlay: Self) -> Self {
        let sub = overlay.sub_modifier | self.sub_modifier;
        Self {
            fg: overlay.fg.or(self.fg),
            bg: overlay.bg.or(self.bg),
            underline_color: overlay.underline_color.or(self.underline_color),
            add_modifier: {
                let mut modifier = overlay.add_modifier | self.add_modifier;
                modifier.remove(sub);
                modifier
            },
            sub_modifier: sub,
        }
    }

    /// Sets the [`Style::fg`] property on the `StyledText`
    #[must_use = "its a builder pattern function"]
    pub fn fg(mut self, fg: impl Into<Option<Color>>) -> Self {
        self.fg = fg.into();
        self
    }

    /// Sets the [`Style::bg`] property on the `StyledText`
    #[must_use = "its a builder pattern function"]
    pub fn bg(mut self, bg: impl Into<Option<Color>>) -> Self {
        self.bg = bg.into();
        self
    }

    /// Sets the [`Style::underline_color`] property on the `StyledText`
    #[must_use = "its a builder pattern function"]
    pub fn underline_color(
        mut self,
        underline_color: impl Into<Option<Color>>,
    ) -> Self {
        self.underline_color = underline_color.into();
        self
    }

    /// Appends to the the [`Style::add_modifier`] property on the `StyledText`
    #[must_use = "its a builder pattern function"]
    pub fn with_modifier(mut self, modifier: Modifier) -> Self {
        self.add_modifier |= modifier;
        self.sub_modifier.remove(modifier);
        self
    }

    /// Appends to the the [`Style::sub_modifier`] property on the
    /// `StyledText`
    #[must_use = "its a builder pattern function"]
    pub fn without_modifier(mut self, modifier: Modifier) -> Self {
        self.sub_modifier |= modifier;
        self.add_modifier.remove(modifier);
        self
    }

    /// Swaps the `fg` and `bg`
    pub fn swapped(mut self) -> Self {
        mem::swap(&mut self.fg, &mut self.bg);
        self
    }

    /// Makes sure the colours are fully realized, and are not `None`
    pub fn realize_color(mut self) -> Self {
        if self.fg.is_none() {
            self.fg = Some(Color::Reset);
        }
        if self.bg.is_none() {
            self.bg = Some(Color::Reset);
        }
        if self.underline_color.is_none() {
            self.underline_color = Some(Color::Reset);
        }

        self
    }

    funcs!();
}

/// Represents a terminal colour
#[derive(Clone, Debug, Copy, PartialEq, Eq)]
#[cfg_attr(
    feature = "serde",
    derive(serde_with::DeserializeFromStr, serde_with::SerializeDisplay)
)]
pub enum Color {
    /// Resets the colour
    Reset,

    /// Black
    Black,
    /// Red
    Red,
    /// Green
    Green,
    /// Yellow
    Yellow,
    /// Blue
    Blue,
    /// Magenta
    Magenta,
    /// Cyan
    Cyan,
    /// White
    White,

    /// Bright Black
    BrightBlack,
    /// Bright Red
    BrightRed,
    /// Bright Green
    BrightGreen,
    /// Bright Yellow
    BrightYellow,
    /// Bright Blue
    BrightBlue,
    /// Bright Magenta
    BrightMagenta,
    /// Bright Cyan
    BrightCyan,
    /// Bright White
    BrightWhite,

    /// An RGB value
    Rgb(u8, u8, u8),

    /// An indexed ansi value -
    /// see [Wikipedia on indexed ansi](https://en.wikipedia.org/wiki/ANSI_escape_code#8-bit)
    Indexed(u8),
}

/// An error for when a colour can't be parsed in the `FromStr` impl for [`Color`]
#[derive(Debug)]
pub struct UnrecognisedColor(String);

impl Display for UnrecognisedColor {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str("unrecognised color: \"")?;
        f.write_str(&self.0)?;
        f.write_str("\"")
    }
}

impl Display for Color {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Color::Reset => f.write_str("reset"),

            Color::Black => f.write_str("black"),
            Color::Red => f.write_str("red"),
            Color::Green => f.write_str("green"),
            Color::Yellow => f.write_str("yellow"),
            Color::Blue => f.write_str("blue"),
            Color::Magenta => f.write_str("magenta"),
            Color::Cyan => f.write_str("cyan"),
            Color::White => f.write_str("white"),

            Color::BrightBlack => f.write_str("bright_black"),
            Color::BrightRed => f.write_str("bright_red"),
            Color::BrightGreen => f.write_str("bright_green"),
            Color::BrightYellow => f.write_str("bright_yellow"),
            Color::BrightBlue => f.write_str("bright_blue"),
            Color::BrightMagenta => f.write_str("bright_magenta"),
            Color::BrightCyan => f.write_str("bright_cyan"),
            Color::BrightWhite => f.write_str("bright_white"),

            Color::Rgb(r, g, b) => f.write_str(&format!("#{r:x}{g:x}{b:x})")),
            Color::Indexed(i) => {
                f.write_str("#")?;
                <u8 as LowerHex>::fmt(i, f)
            }
        }
    }
}

impl FromStr for Color {
    type Err = UnrecognisedColor;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "reset" => Ok(Color::Reset),

            "black" => Ok(Color::Black),
            "red" => Ok(Color::Red),
            "green" => Ok(Color::Green),
            "yellow" => Ok(Color::Yellow),
            "blue" => Ok(Color::Blue),
            "magenta" => Ok(Color::Magenta),
            "cyan" => Ok(Color::Cyan),
            "white" => Ok(Color::White),

            "bright_black" => Ok(Color::BrightBlack),
            "bright_red" => Ok(Color::BrightRed),
            "bright_green" => Ok(Color::BrightGreen),
            "bright_yellow" => Ok(Color::BrightYellow),
            "bright_blue" => Ok(Color::BrightBlue),
            "bright_magenta" => Ok(Color::BrightMagenta),
            "bright_cyan" => Ok(Color::BrightCyan),
            "bright_white" => Ok(Color::BrightWhite),

            s => {
                // Parse rgb or indexed colours
                if s.starts_with('#') {
                    if s.len() == 7 {
                        // Get str
                        let r = &s[1..=2];
                        let g = &s[3..=4];
                        let b = &s[5..=6];

                        // Convert from hex
                        let r = u8::from_str_radix(r, 16).ok();
                        let g = u8::from_str_radix(g, 16).ok();
                        let b = u8::from_str_radix(b, 16).ok();

                        if let Some(((r, g), b)) = r.zip(g).zip(b) {
                            Ok(Color::Rgb(r, g, b))
                        } else {
                            Err(UnrecognisedColor(s.to_string()))
                        }
                    } else if s.len() == 3 {
                        let i = &s[1..=2];
                        match u8::from_str_radix(i, 16) {
                            Ok(i) => Ok(Color::Indexed(i)),
                            Err(_) => Err(UnrecognisedColor(s.to_owned())),
                        }
                    } else {
                        Err(UnrecognisedColor(s.to_string()))
                    }
                } else {
                    Err(UnrecognisedColor(s.to_string()))
                }
            }
        }
    }
}

#[cfg(feature = "image")]
impl<P> From<P> for Color
where
    P: image::Pixel,
    u8: From<<P as image::Pixel>::Subpixel>,
{
    fn from(them: P) -> Self {
        let them = image::Pixel::to_rgb(&them);
        Color::Rgb(them[0].into(), them[1].into(), them[2].into())
    }
}

impl Color {
    /// Creates a colour from an ansi escape
    pub fn from_ansi(ansi: u8) -> Option<Self> {
        match ansi {
            0 | 39 | 49 => Some(Self::Reset),

            30 | 40 => Some(Self::Black),
            31 | 41 => Some(Self::Red),
            32 | 42 => Some(Self::Green),
            33 | 43 => Some(Self::Yellow),
            34 | 44 => Some(Self::Blue),
            35 | 45 => Some(Self::Magenta),
            36 | 46 => Some(Self::Cyan),
            37 | 47 => Some(Self::White),

            90 | 100 => Some(Self::BrightBlack),
            91 | 101 => Some(Self::BrightRed),
            92 | 102 => Some(Self::BrightGreen),
            93 | 103 => Some(Self::BrightYellow),
            94 | 104 => Some(Self::BrightBlue),
            95 | 105 => Some(Self::BrightMagenta),
            96 | 106 => Some(Self::BrightCyan),
            97 | 107 => Some(Self::BrightWhite),

            _ => None,
        }
    }
}

#[cfg(feature = "crossterm")]
impl From<Color> for CrossColor {
    #[cfg(feature = "crossterm")]
    fn from(them: Color) -> CrossColor {
        match them {
            Color::Reset => CrossColor::Reset,

            Color::Black => CrossColor::Black,
            Color::Red => CrossColor::DarkRed,
            Color::Green => CrossColor::DarkGreen,
            Color::Yellow => CrossColor::DarkYellow,
            Color::Blue => CrossColor::DarkBlue,
            Color::Magenta => CrossColor::DarkMagenta,
            Color::Cyan => CrossColor::DarkCyan,
            Color::White => CrossColor::Grey,

            Color::BrightBlack => CrossColor::DarkGrey,
            Color::BrightRed => CrossColor::Red,
            Color::BrightGreen => CrossColor::Green,
            Color::BrightYellow => CrossColor::Yellow,
            Color::BrightBlue => CrossColor::Blue,
            Color::BrightMagenta => CrossColor::Magenta,
            Color::BrightCyan => CrossColor::Cyan,
            Color::BrightWhite => CrossColor::White,

            Color::Rgb(r, g, b) => CrossColor::Rgb { r, g, b },
            Color::Indexed(n) => CrossColor::AnsiValue(n),
        }
    }
}

bitflags::bitflags! {
    /// A bitflags of modifiers a cell can have
    #[derive(Default, Clone, Copy,PartialEq, Eq, PartialOrd, Ord, Debug, Hash)]
    #[cfg_attr(feature = "serde", derive(serde::Deserialize, serde::Serialize), serde(transparent))]
    pub struct Modifier: u32 {
        /// No style
        const NONE              = 0;
        /// Bold (Strong) text
        const BOLD              = 2_u32.pow(1);
        /// Dim text
        const DIM               = 2_u32.pow(2);
        /// Italics text
        const ITALIC            = 2_u32.pow(3);
        /// Underlines text
        const UNDERLINED        = 2_u32.pow(4);
        /// Sets the text to blink slowly
        const SLOW_BLINK        = 2_u32.pow(5);
        /// Sets the text to blink rapidly
        const RAPID_BLINK       = 2_u32.pow(6);
        /// Sets the background to be the forground and visa-versa
        const REVERSED          = 2_u32.pow(7);
        /// Makes the text invisible
        const HIDDEN            = 2_u32.pow(8);
        /// Crosses the text.
        const CROSSED_OUT       = 2_u32.pow(9);
        /// Doubly nderlines text
        const DOUBLE_UNDERLINED = 2_u32.pow(10);
        /// Undercurled text
        const UNDERCURLED       = 2_u32.pow(11);
        /// Underdotted text
        const UNDERDOTTED       = 2_u32.pow(12);
        /// Underdashed text
        const UNDERDASHED       = 2_u32.pow(12);
    }
}

#[cfg(feature = "crossterm")]
impl From<Modifier> for Vec<CrossAttribute> {
    /// Gives the crossterm version of a modifier.
    fn from(them: Modifier) -> Self {
        let mut vec = Vec::new();
        if them.contains(Modifier::BOLD) {
            vec.push(CrossAttribute::Bold);
        }
        if them.contains(Modifier::DIM) {
            vec.push(CrossAttribute::Dim);
        }
        if them.contains(Modifier::ITALIC) {
            vec.push(CrossAttribute::Italic);
        }
        if them.contains(Modifier::UNDERLINED) {
            vec.push(CrossAttribute::Underlined);
        }
        if them.contains(Modifier::SLOW_BLINK) {
            vec.push(CrossAttribute::SlowBlink);
        }
        if them.contains(Modifier::RAPID_BLINK) {
            vec.push(CrossAttribute::RapidBlink);
        }
        if them.contains(Modifier::REVERSED) {
            vec.push(CrossAttribute::Reverse);
        }
        if them.contains(Modifier::HIDDEN) {
            vec.push(CrossAttribute::Hidden);
        }
        if them.contains(Modifier::CROSSED_OUT) {
            vec.push(CrossAttribute::CrossedOut);
        }
        if them.contains(Modifier::DOUBLE_UNDERLINED) {
            vec.push(CrossAttribute::DoubleUnderlined);
        }
        if them.contains(Modifier::UNDERCURLED) {
            vec.push(CrossAttribute::Undercurled);
        }
        if them.contains(Modifier::UNDERDOTTED) {
            vec.push(CrossAttribute::Underdotted);
        }
        if them.contains(Modifier::UNDERDASHED) {
            vec.push(CrossAttribute::Underdashed);
        }
        vec
    }
}

/// Text with a style attached to it.
#[derive(Clone, Default, PartialEq, Eq, Debug)]
pub struct StyledText<'a> {
    /// The style of the text
    pub style: Style,
    /// The text itself
    pub text: Cow<'a, str>,
}

impl<'a, T> From<T> for StyledText<'a>
where
    Cow<'a, str>: From<T>,
{
    fn from(text: T) -> Self {
        Self {
            style: Style::default(),
            text: text.into(),
        }
    }
}

impl StyledText<'_> {
    /// Sets the style
    #[must_use = "its a builder pattern function"]
    pub fn style(mut self, style: Style) -> Self {
        self.style = style;
        self
    }

    /// Sets the [`Style::fg`] property on the `StyledText`
    #[must_use = "its a builder pattern function"]
    pub fn fg(mut self, fg: impl Into<Option<Color>>) -> Self {
        self.style.fg = fg.into();
        self
    }

    /// Sets the [`Style::bg`] property on the `StyledText`
    #[must_use = "its a builder pattern function"]
    pub fn bg(mut self, bg: impl Into<Option<Color>>) -> Self {
        self.style.bg = bg.into();
        self
    }

    /// Sets the [`Style::underline_color`] property on the `StyledText`
    #[must_use = "its a builder pattern function"]
    pub fn underline_color(
        mut self,
        underline_color: impl Into<Option<Color>>,
    ) -> Self {
        self.style.underline_color = underline_color.into();
        self
    }

    /// Appends to the the [`Style::add_modifier`] property on the `StyledText`
    #[must_use = "its a builder pattern function"]
    pub fn with_modifier(mut self, modifier: Modifier) -> Self {
        self.style.add_modifier |= modifier;
        self.style.sub_modifier.remove(modifier);
        self
    }

    /// Appends to the the [`Style::sub_modifier`] property on the
    /// `StyledText`
    #[must_use = "its a builder pattern function"]
    pub fn without_modifier(mut self, modifier: Modifier) -> Self {
        self.style.sub_modifier |= modifier;
        self.style.add_modifier.remove(modifier);
        self
    }

    funcs!();
}
