//! Terminal representation.

use std::{
    fmt::{self, Display},
    mem,
};

use le::layout::{Rect, Vec2};
use unicode_segmentation::UnicodeSegmentation;
use unicode_width::UnicodeWidthStr;

use super::{Style, StyledText};

/// The text stored in a cell
#[derive(Default, Debug, Clone, PartialEq, Eq)]
pub enum CellText {
    /// Only used if you start with the zero width chars
    String(String),
    /// A character
    Char(char),
    /// Literally nothing
    #[default]
    None,
}

impl CellText {
    /// Determines if the cell is empty
    pub fn is_empty(&self) -> bool {
        match self {
            CellText::String(x) => x.is_empty(),
            CellText::Char(_) => false,
            CellText::None => true,
        }
    }
}

impl Display for CellText {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            CellText::String(x) => x.fmt(f),
            CellText::Char(x) => x.fmt(f),
            CellText::None => ' '.fmt(f),
        }
    }
}

/// A single cell on the buffer with text and style.
#[derive(Debug, Clone, Default, PartialEq, Eq)]
pub struct Cell {
    /// The text.
    ///
    /// This is a string because certain combinations
    /// of unicode characters may still fit in a cell.
    pub text: CellText,
    /// The style of the cell
    pub style: Style,
}

/// A buffer of cells storing its size and text.
#[derive(Debug, Clone)]
pub struct Buffer {
    /// The size of the buffer
    size: Vec2,
    /// The text in the buffer
    text: Vec<Cell>,
}

/// A immutable reference to a subsection of a `Buffer`
#[derive(Debug, Clone)]
pub struct BufferSlice<'a> {
    /// The buffer which this points too
    buffer: &'a Buffer,
    /// The 'slice' of that buffer we reference
    rect: Rect,
}

/// A mutable reference to a subsection of a `Buffer`
#[derive(Debug)]
pub struct BufferSliceMut<'a> {
    /// The buffer which this points too
    buffer: &'a mut Buffer,
    /// The 'slice' of that buffer we reference
    rect: Rect,
}

impl Buffer {
    /// Converts this to a [`BufferSlice`]
    pub fn as_slice(&self) -> BufferSlice<'_> {
        BufferSlice {
            rect: Rect {
                start: Vec2::new(0, 0),
                size: self.size,
            },
            buffer: self,
        }
    }

    /// Converts this to a [`BufferSliceMut`]
    pub fn as_mut_slice(&mut self) -> BufferSliceMut<'_> {
        BufferSliceMut {
            rect: Rect {
                start: Vec2::new(0, 0),
                size: self.size,
            },
            buffer: self,
        }
    }
}

impl Buffer {
    /// Creates a new buffer of the given size.
    ///
    /// The size will normally be the size of the terminal.
    pub fn new(size: Vec2) -> Self {
        Self {
            size,
            text: vec![Cell::default(); size.y * size.x],
        }
    }

    /// Gets all the text
    pub fn text(&self) -> &[Cell] {
        &self.text
    }

    /// Reduces the buffer to size `clip`
    ///
    /// ```txt
    // +-------+-------+
    // |       |       |
    // | clip  |       |
    // +-------+       |
    // | self          |
    // +---------------+
    // ```
    pub fn clip(&mut self, clip: Vec2) {
        self.crop(Rect {
            start: Vec2::new(0, 0),
            size: clip,
        })
    }

    /// Crops the buffer to a specific rect
    pub fn crop(&mut self, crop: Rect) {
        for y in 0..crop.size.y {
            let src_start = (crop.start.y + y) * self.size.x;
            let src_end = src_start + crop.size.x;
            let src = src_start..src_end;

            let dst_start = y * crop.size.x;
            let dst_end = dst_start + crop.size.x;
            let dst = dst_start..dst_end;

            for (src_i, dst_i) in src.zip(dst) {
                if dst_i < self.text.len() {
                    if src_i < self.text.len() {
                        if src_i != dst_i {
                            self.text.swap(src_i, dst_i);
                            self.text[src_i] = Cell::default();
                        }
                    } else {
                        self.text[dst_i] = Cell::default();
                    }
                }
            }
        }

        // self.text.truncate(crop.size.x * crop.size.y);
        // *self = new_buffer;
    }
}

impl<'a> BufferSlice<'a> {
    /// Gets the size of the Buffer
    pub fn size(&self) -> Vec2 {
        self.rect.size
    }

    /// Gets the start of the rect to the Buffer this slices.
    pub fn start(&self) -> Vec2 {
        self.rect.start
    }

    /// Gets a specfic cell
    pub fn get(&self, pos: Vec2) -> Option<&'a Cell> {
        let pos = pos + self.rect.start;
        if self.rect.contains(pos) {
            self.buffer.text.get(pos.x + pos.y * self.buffer.size.x)
        } else {
            None
        }
    }

    /// Gets a sub slice of this
    pub fn sub_slice(&self, rect: Rect) -> Option<BufferSlice<'a>> {
        if rect.end().x <= self.rect.size.x && rect.end().y <= self.rect.size.y
        {
            Some(BufferSlice {
                buffer: self.buffer,
                rect: Rect {
                    start: self.rect.start + rect.start,
                    size: rect.size,
                },
            })
        } else {
            None
        }
    }
}

impl<'a> BufferSliceMut<'a> {
    /// Gets an immutable version of self
    pub fn as_slice<'b>(&'b self) -> BufferSlice<'b>
    where
        'a: 'b,
    {
        BufferSlice {
            buffer: self.buffer,
            rect: self.rect,
        }
    }

    /// Gets the size of the Buffer
    pub fn size(&self) -> Vec2 {
        self.as_slice().size()
    }

    /// Gets a specfic cell
    pub fn get(&self, pos: Vec2) -> Option<&Cell> {
        let slice = self.as_slice();
        slice.get(pos)
    }

    /// Clears the buffer, writing all cells as empty.
    pub fn clear(&mut self) {
        for x in self.rect.start.x..self.rect.end().x {
            for y in self.rect.start.y..self.rect.end().y {
                self.buffer.text[x + y * self.buffer.size.x] = Cell::default();
            }
        }
    }

    /// Gets a mutable version of a cell
    pub fn get_mut(&mut self, pos: Vec2) -> Option<&mut Cell> {
        let pos = pos + self.rect.start;
        if self.rect.contains(pos) {
            self.buffer.text.get_mut(pos.x + pos.y * self.buffer.size.x)
        } else {
            None
        }
    }

    /// Writes the text to the buffer. The text should **NOT**
    /// be multiline. Multiline text will not be handled correctly.
    pub fn write<'b>(&mut self, pos: Vec2, text: impl Into<StyledText<'b>>) {
        self.write_span(pos, &text.into());
    }

    /// Writes the span to the buffer. The text should **NOT**
    /// be multiline. Multiline text will not be handled correctly.
    pub fn write_span(&mut self, mut pos: Vec2, text: &StyledText) {
        let graphemes = text.text.graphemes(true);
        for grapheme in graphemes {
            if let Some(previous) = self.get_mut(pos) {
                let mut grapheme_chars = grapheme.chars();
                let char1 = grapheme_chars.next();
                let char2 = grapheme_chars.next();
                match (char1, char2) {
                    (Some(_char1), Some(_char2)) => {
                        previous.text = CellText::String(grapheme.to_owned());
                    }
                    (Some(char1), None) => {
                        previous.text = CellText::Char(char1);
                    }
                    _ => (),
                }
                previous.style = previous.style.overlay(text.style);
            }
            pos.x += grapheme.width();
        }
    }

    /// Writes another buffer over this buffer
    pub fn write_buffer(&mut self, mut them: Buffer, pos: Vec2) {
        let mut them = them.as_mut_slice();
        for x in 0..them.size().x {
            for y in 0..them.size().y {
                let rel_pos = Vec2::new(x, y);
                let cell =
                    them.get_mut(rel_pos).map(mem::take).unwrap_or_default();
                let cell_pos = pos + rel_pos;

                if let Some(previous) = self.get_mut(cell_pos) {
                    previous.text = cell.text;
                    previous.style = previous.style.overlay(cell.style);
                };
            }
        }
    }

    /// Writes another buffer over this buffer, only cloning when need be
    pub fn write_buffer_slice(&mut self, them: &BufferSlice, pos: Vec2) {
        for x in 0..them.size().x {
            for y in 0..them.size().y {
                let rel_pos = Vec2::new(x, y);
                let cell = them.get(rel_pos).unwrap().clone();
                let cell_pos = pos + rel_pos;

                if let Some(previous) = self.get_mut(cell_pos) {
                    previous.text = cell.text;
                    previous.style = previous.style.overlay(cell.style);
                };
            }
        }
    }

    /// Writes a character to the buffer
    pub fn write_char(&mut self, c: char, pos: Vec2) {
        if let Some(cell) = self.get_mut(pos) {
            cell.text = CellText::Char(c);
        }
    }

    /// Style a specific cell
    pub fn style_cell(&mut self, style: Style, pos: Vec2) {
        if let Some(cell) = self.get_mut(pos) {
            cell.style = cell.style.overlay(style);
        }
    }

    /// Gets a sub slice of this, mutably
    pub fn sub_mut_slice<'b>(
        &'b mut self,
        rect: Rect,
    ) -> Option<BufferSliceMut<'b>>
    where
        'a: 'b,
    {
        let mut rect_abs = rect;
        rect_abs.start += self.rect.start;
        let contained = {
            rect_abs.end().x <= self.rect.end().x
                && rect_abs.end().y <= self.rect.end().y
                && rect_abs.end().x <= self.rect.end().x
                && rect_abs.end().y <= self.rect.end().y
        };
        if contained {
            Some(BufferSliceMut {
                buffer: self.buffer,
                rect: Rect {
                    start: self.rect.start + rect.start,
                    size: rect.size,
                },
            })
        } else {
            None
        }
    }

    /// Gets a sub slice of this, mutably.
    ///
    /// If the `rect` is too big then it will return a buffer
    /// of a smaller size
    pub fn sub_mut_slice_clipped<'b>(
        &'b mut self,
        rect: Rect,
    ) -> Option<BufferSliceMut<'b>>
    where
        'a: 'b,
    {
        if self.rect.contains_inclusive(rect.start + self.rect.start) {
            let max_size = self.rect.size - rect.start;
            Some(
                self.sub_mut_slice(Rect {
                    start: rect.start,
                    size: Vec2::new(
                        max_size.x.min(rect.size.x),
                        max_size.y.min(rect.size.y),
                    ),
                })
                .unwrap(),
            )
        } else {
            None
        }
    }

    /// Gets the start of the rect to the Buffer this slices.
    pub fn start(&self) -> Vec2 {
        self.rect.start
    }
}
