//! Module housing the `progress_bar` widget

use std::array;

use le::layout::Vec2;
use le::{Layout, MinimumNatural, Orientation};

use crate::buffer::BufferSliceMut;
use crate::{prelude::*, widgets::Filler, Rect, StyledText, Widget};

/// A progress bar to display a value between `0` and
/// [`value`](ProgressBar::value)
pub struct ProgressBar<'a, const E: usize = 1> {
    /// The maximum value which the progress bar can go up to
    pub total: f64,
    /// The value which the progress bar displays.
    ///
    /// This value should always be less than or eqal to
    /// total whilst being positive.
    pub value: f64,
    /// The foreground (the parts less than [`value`](ProgressBar::value)) of
    /// the bar
    pub fg: Vec<StyledText<'a>>,
    /// The edge (the separator between [`fg`](ProgressBar::fg) and
    /// [`bg`](ProgressBar::bg) of the bar. Unlike the others
    /// each value in this array is a different text which will be shown
    /// at differnet completions.
    pub edge: [StyledText<'a>; E],
    /// The background (the parts more than [`value`](ProgressBar::value) of
    /// the bar
    pub bg: Vec<StyledText<'a>>,
    /// The direction of the bar
    pub orientation: Orientation,
}

impl<'a, const E: usize> Default for ProgressBar<'a, E> {
    fn default() -> Self {
        ProgressBar {
            fg: vec![" ".styled().bg_green()],
            bg: vec!["─".styled().cyan().bold()],
            edge: array::from_fn(|_| "▌".styled().green()),
            total: 1.0,
            value: 0.0,
            orientation: Orientation::Horizontal,
        }
    }
}

impl<'a, const E: usize> ProgressBar<'a, E> {
    /// Creates a new [`ProgressBar`]
    ///
    /// By default it has a green foreground and a cyan line as the background
    /// to indicate the maximum value. It has the total of 1.0 and a value of
    /// 0.0, whilst being Horizontal.
    pub fn new() -> Self {
        Default::default()
    }

    /// Sets the [`ProgressBar::total`] field.
    pub fn total(mut self, total: f64) -> Self {
        self.total = total;
        self
    }

    /// Sets the [`ProgressBar::value`] field.
    pub fn value(mut self, value: f64) -> Self {
        self.value = value;
        self
    }

    /// Sets the [`ProgressBar::fg`] field.
    pub fn fg(mut self, fg: Vec<StyledText<'a>>) -> Self {
        self.fg = fg;
        self
    }

    /// Sets the [`ProgressBar::bg`] field.
    pub fn bg(mut self, bg: Vec<StyledText<'a>>) -> Self {
        self.bg = bg;
        self
    }

    /// Sets the [`ProgressBar::edge`] field.
    pub fn edge(mut self, edge: [StyledText<'a>; E]) -> Self {
        self.edge = edge;
        self
    }

    /// Sets the [`ProgressBar::orientation`] field to [`Orientation::Vertical`]
    pub fn vertical(mut self) -> Self {
        self.orientation = Orientation::Vertical;
        self
    }
}

impl<const E: usize> Layout for ProgressBar<'_, E> {
    fn width_for_height(&self, _width: usize) -> MinimumNatural<usize> {
        MinimumNatural::same(1)
    }

    fn height_for_width(&self, _height: usize) -> MinimumNatural<usize> {
        MinimumNatural::same(1)
    }

    fn prefered_size(&self) -> MinimumNatural<Vec2> {
        MinimumNatural::same(Vec2::new(1, 1))
    }

    fn prefered_size_of_container(
        &self,
        container: Vec2,
    ) -> MinimumNatural<Vec2> {
        MinimumNatural {
            minimum: Vec2::new(1, 1),
            natural: match self.orientation {
                Orientation::Vertical => Vec2::new(1, container.y),
                Orientation::Horizontal => Vec2::new(container.x, 1),
            },
        }
    }
}

impl<const E: usize> Widget for ProgressBar<'_, E> {
    fn render<'a>(&self, buffer: &mut BufferSliceMut<'_>) {
        let total = if self.total == 0.0 { 1.0 } else { self.total };
        let value = self.value / total;

        let fg_filler = Filler::new_texts(self.fg.clone()).can_cut(true);
        let bg_filler = Filler::new_texts(self.bg.clone()).can_cut(true);

        let prim_dir = match self.orientation {
            Orientation::Vertical => buffer.size().y,
            Orientation::Horizontal => buffer.size().x,
        };
        let abs_value = (prim_dir as f64 * value) as usize;

        let edge_frac = (value * (prim_dir as f64)).fract();
        let edge = &self.edge[(edge_frac * (E as f64)) as usize];

        match self.orientation {
            Orientation::Vertical => {
                let fg_rect = Rect::new(0, 0, buffer.size().x, abs_value);
                let fg_buffer = &mut buffer.sub_mut_slice(fg_rect).unwrap();
                fg_filler.render(fg_buffer);

                let bg_rect = Rect::new(
                    0,
                    abs_value + 1,
                    buffer.size().x,
                    buffer.size().y.saturating_sub(abs_value + 1),
                );
                let mut bg_buffer = buffer.sub_mut_slice(bg_rect).unwrap();
                bg_filler.render(&mut bg_buffer);

                buffer.write_span(Vec2::new(0, abs_value), edge);
            }
            Orientation::Horizontal => {
                let fg_rect = Rect::new(0, 0, abs_value, buffer.size().y);
                let fg_buffer = &mut buffer.sub_mut_slice(fg_rect).unwrap();
                fg_filler.render(fg_buffer);

                let bg_rect = Rect::new(
                    abs_value + 1,
                    0,
                    buffer.size().x.saturating_sub(abs_value + 1),
                    buffer.size().y,
                );
                let mut bg_buffer = buffer.sub_mut_slice(bg_rect).unwrap();
                bg_filler.render(&mut bg_buffer);

                buffer.write_span(Vec2::new(abs_value, 0), edge);
            }
        }
    }
}
