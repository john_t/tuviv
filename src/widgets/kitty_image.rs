//! A module working for images which are displayed with the Kitty graphics procotol.

use std::{
    io::{stdout, Cursor},
    num::NonZeroU32,
};

use crossterm::terminal::window_size;
use image::{
    imageops::FilterType, DynamicImage, GenericImageView, ImageResult,
};
use kitty_image::{
    Action, ActionPut, ActionTransmission, Command, Format, Medium, Quietness,
    WrappedCommand, ID, UNICODE_DIACRITICS,
};
use le::{layout::Vec2, Layout, MinimumNatural};

use crate::{
    buffer::{BufferSliceMut, CellText},
    Color, Widget,
};

use super::image::{impl_layout_for_image, ImageBufferB, ImageLayout};

/// A widget which renders an image using the kitty graphics protocol
///
/// *requires the `kitty` dependency*
pub struct KittyImage<'a, I: GenericImageView> {
    /// The image to render
    pub image: ImageBufferB<'a, I>,
    /// The ID of the image
    pub id: NonZeroU32,
    /// How should the image be resized?
    pub resize_filter: FilterType,
}

impl<'a, I: GenericImageView> KittyImage<'a, I> {
    /// Creates a new [`Image`]
    pub fn new<T: Into<ImageBufferB<'a, I>>>(
        image: T,
        id: NonZeroU32,
    ) -> ImageResult<Self> {
        Ok(Self {
            image: image.into(),
            id,
            resize_filter: FilterType::Nearest,
        })
    }

    /// Sets the [`resize_filter`] property on this
    pub fn resize_filter(mut self, filter: FilterType) -> Self {
        self.resize_filter = filter;
        self
    }
}

impl<'a, I: GenericImageView> ImageLayout for KittyImage<'a, I> {
    /// Gets the dimensions of this
    fn dimensions(&self) -> Vec2<usize> {
        Vec2::new(self.image.width() as usize, self.image.height() as usize)
    }
}

impl<'a, I: GenericImageView> Layout for KittyImage<'a, I> {
    impl_layout_for_image!();
}

impl<'a> Widget for KittyImage<'a, DynamicImage>
// where
// [<<I as GenericImageView>::Pixel as Pixel>::Subpixel]:
//     EncodableLayout + 'static,
// <I as GenericImageView>::Pixel: 'static,
// <I as GenericImageView>::Pixel: PixelWithColorType,
{
    fn render(&self, buffer: &mut BufferSliceMut<'_>) {
        // Fixes a crash that occurs on zero sized images
        if buffer.size().x == 0 || buffer.size().y == 0 {
            return;
        }

        let is_tmux = is_tmux();
        let size = self.prefered_size_of_container(buffer.size()).natural;

        let win_size = window_size().unwrap();
        let px_per_cell_y = win_size.height as f32 / win_size.rows as f32;
        let px_per_cell_x = win_size.width as f32 / win_size.columns as f32;

        let resized = image::imageops::resize(
            &*self.image,
            (size.x as f32 * px_per_cell_x) as u32,
            (size.y as f32 * px_per_cell_y) as u32,
            self.resize_filter,
        );

        let action = Action::TransmitAndDisplay(
            ActionTransmission {
                format: Format::Png,
                medium: Medium::Direct,
                width: resized.width(),
                height: resized.height(),
                ..Default::default()
            },
            ActionPut {
                unicode_placeholder: true,
                columns: size.x as u32,
                rows: size.y as u32,

                ..Default::default()
            },
        );

        // Encode the image
        let mut bytes: Vec<u8> = Vec::new();
        resized
            .write_to(
                &mut Cursor::new(&mut bytes),
                image::ImageOutputFormat::Png,
            )
            .unwrap();

        let mut command = Command::new(action);
        command.payload = bytes.as_slice().into();
        command.id = Some(ID(self.id));
        command.quietness = Quietness::SuppressAll;
        let mut command = WrappedCommand::new(command);

        // Write the command
        if is_tmux {
            command.double_escape = true;
            print!("\x1bPtmux;");
            command.send_chunked(&mut stdout()).unwrap();
            print!("\x1b\\");
        } else {
            command.send_chunked(&mut stdout()).unwrap();
        }

        for (y, y_dia) in UNICODE_DIACRITICS.iter().take(size.y).enumerate() {
            for (x, x_dia) in UNICODE_DIACRITICS.iter().take(size.x).enumerate()
            {
                let p = Vec2::new(x, y);
                let Some(cell) = buffer.get_mut(p) else {
                    continue;
                };
                cell.text =
                    CellText::String(format!("\u{10eeee}{}{}", y_dia, x_dia));
                cell.style.fg = Some(Color::Indexed(self.id.get() as u8));
            }
        }
    }
}

/// Determines if we are in a Tmux pane
fn is_tmux() -> bool {
    std::env::var("TMUX").is_ok()
}
