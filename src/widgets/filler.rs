//! Houses the [`Filler`] widget

use le::Layout;
use le::{layout::Vec2, MinimumNatural};
use unicode_width::{UnicodeWidthChar, UnicodeWidthStr};

use crate::buffer::BufferSliceMut;
use crate::{StyledText, Widget};

/// Repeats the [`Filler::repeat`] field to fill up the space given to it.
///
/// By itself it requests only to take up 1x1 amount of space.
pub struct Filler<'a> {
    /// The text to repeat
    pub repeat: Vec<StyledText<'a>>,
    /// Determines if we can 'cut' the text short
    pub can_cut: bool,
}

impl<'a> Filler<'a> {
    /// Creates a new filler from a single [`StyledText`]
    pub fn new(repeat: StyledText<'a>) -> Self {
        Filler {
            repeat: vec![repeat],
            can_cut: false,
        }
    }

    /// Creates a new filler from multiple [`StyledText`]s
    pub fn new_texts(repeat: Vec<StyledText<'a>>) -> Self {
        Filler {
            repeat,
            can_cut: false,
        }
    }

    /// A builder pattern for the `can_cut` property.
    pub fn can_cut(mut self, can_cut: bool) -> Self {
        self.can_cut = can_cut;
        self
    }
}

impl Layout for Filler<'_> {
    fn width_for_height(&self, _width: usize) -> MinimumNatural<usize> {
        MinimumNatural::same(1)
    }

    fn height_for_width(&self, _height: usize) -> MinimumNatural<usize> {
        MinimumNatural::same(1)
    }

    fn prefered_size(&self) -> MinimumNatural<Vec2> {
        MinimumNatural::same(Vec2::new(1, 1))
    }

    fn prefered_size_of_container(
        &self,
        container: Vec2,
    ) -> MinimumNatural<Vec2> {
        MinimumNatural {
            minimum: Vec2::new(1, 1),
            natural: container,
        }
    }
}

impl Widget for Filler<'_> {
    fn render<'a>(&self, buffer: &mut BufferSliceMut<'_>) {
        let mut x_pos = 0;
        'main_loop: for span in self.repeat.iter().cycle() {
            if x_pos + span.text.width() > buffer.size().x {
                // Check to see if we can write only a bit of this
                if self.can_cut {
                    for chr in span.text.chars() {
                        if x_pos >= buffer.size().x {
                            break 'main_loop;
                        }

                        let span_chr = StyledText {
                            style: span.style,
                            text: chr.to_string().into(),
                        };

                        for y in 0..buffer.size().y {
                            buffer.write_span(Vec2::new(x_pos, y), &span_chr);
                        }

                        x_pos += chr.width().unwrap_or(1);
                    }
                } else {
                    break;
                }
            }

            for y in 0..buffer.size().y {
                buffer.write_span(Vec2::new(x_pos, y), span);
            }

            x_pos += span.text.width();
        }
    }
}
