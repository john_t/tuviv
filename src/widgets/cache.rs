//! Houses the [`Cache`] widget

use std::{cell::RefCell, collections::BTreeMap, rc::Rc};

use le::{layout::Vec2, Layout, MinimumNatural};

use crate::{buffer::BufferSliceMut, Buffer, Widget};

/// The type used to cache for the `Cache` widget
pub type CachedBuffers = Rc<RefCell<BTreeMap<Vec2, Buffer>>>;

/// A widget the caches its content's display.
///
/// This is mainly useful with the [`Image`](super::image::Image) widget.
/// It should only be used if the widget is more CPU intensive then memory
/// intensive. This widget preforms clones.
pub struct Cache<T: Widget> {
    /// The widget to cache
    pub widget: T,

    /// The cache buffer
    pub buffers: CachedBuffers,

    /// The maximum amount of maps to store.
    ///
    /// If set to `0`, then the capacity is infinite.
    ///
    /// For [`KittyImage`] this should always be `1`.
    pub max_capacity: usize,
}

impl<T: Widget> Cache<T> {
    /// Creates a new cache.
    ///
    /// The `buffers` should be made with `Default::default` and
    /// should be stored between frames
    pub fn new(widget: T, buffers: &CachedBuffers) -> Self {
        Self {
            widget,
            buffers: buffers.clone(),
            max_capacity: 4,
        }
    }

    /// Set the [`Self::max_capacity`] property
    pub fn max_capacity(mut self, capacity: usize) -> Self {
        self.max_capacity = capacity;
        self
    }
}

impl<T: Widget> Layout for Cache<T> {
    fn width_for_height(&self, height: usize) -> MinimumNatural<usize> {
        self.widget.width_for_height(height)
    }

    fn height_for_width(&self, width: usize) -> MinimumNatural<usize> {
        self.widget.height_for_width(width)
    }

    fn prefered_size(&self) -> MinimumNatural<Vec2> {
        self.widget.prefered_size()
    }

    fn prefered_size_of_container(
        &self,
        container: Vec2,
    ) -> MinimumNatural<Vec2> {
        self.widget.prefered_size_of_container(container)
    }
}

impl<T: Widget> Widget for Cache<T> {
    fn render(&self, buffer: &mut BufferSliceMut<'_>) {
        let size = buffer.size();
        let mut buffers = self.buffers.borrow_mut();

        // Check if its in the cache
        if let Some(cache) = buffers.get(&size) {
            // If so write it
            buffer.write_buffer_slice(&cache.as_slice(), Vec2::new(0, 0));
        } else {
            // Else create a new buffer
            let mut temp_buffer = Buffer::new(size);
            self.widget.render(&mut temp_buffer.as_mut_slice());

            if buffers.len() >= self.max_capacity {
                buffers.pop_last();
            }

            // Write it
            buffer.write_buffer_slice(&temp_buffer.as_slice(), Vec2::new(0, 0));

            // Cache it
            buffers.insert(size, temp_buffer);
        }
    }
}
