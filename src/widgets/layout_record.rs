//! Houses the [`LayoutRecord`] widget

use std::{cell::Cell, rc::Rc};

use le::{
    layout::{Rect, Vec2},
    Layout, MinimumNatural,
};

use crate::{buffer::BufferSliceMut, Widget};

/// Records the layout of a widget
///
/// This is useful if you wanted to do mouse-input, as you
/// can detect where a widget is as to detect if the mouse is over
/// said widget. See the [mouse](https://gitlab.com/john_t/tuviv/-/blob/master/examples/mouse.rs)
/// example for more.
pub struct LayoutRecord<T: Widget, F: Fn(Rect) -> Rect = fn(Rect) -> Rect> {
    /// The widget to record.
    pub widget: T,
    /// The place to store the record.
    pub record: Rc<Cell<Rect>>,
    /// A mapping to be applied to the record
    pub mapping: F,
}

/// The default [`LayoutRecord::mapping`]
fn mapping(r: Rect) -> Rect {
    r
}

impl<T: Widget> LayoutRecord<T> {
    /// Creates a new [`LayoutRecord`]
    ///
    /// See [`WidgetExt`](crate::prelude::WidgetExt::to_layout_record) for
    /// a builder method
    pub fn new(widget: T, record: Rc<Cell<Rect>>) -> Self {
        Self {
            widget,
            record,
            mapping,
        }
    }
}
impl<T: Widget, F: Fn(Rect) -> Rect> LayoutRecord<T, F> {
    /// Creates a new [`LayoutRecord`] with a [mapping](LayoutRecord::mapping)
    ///
    /// See [`WidgetExt`](crate::prelude::WidgetExt::to_layout_record) for
    /// a builder method
    pub fn with_mapping(widget: T, record: Rc<Cell<Rect>>, mapping: F) -> Self {
        Self {
            widget,
            record,
            mapping,
        }
    }
}

impl<T: Widget, F: Fn(Rect) -> Rect> Layout for LayoutRecord<T, F> {
    fn width_for_height(&self, height: usize) -> MinimumNatural<usize> {
        self.widget.width_for_height(height)
    }

    fn height_for_width(&self, width: usize) -> MinimumNatural<usize> {
        self.widget.height_for_width(width)
    }

    fn prefered_size(&self) -> MinimumNatural<Vec2> {
        self.widget.prefered_size()
    }

    fn prefered_size_of_container(
        &self,
        container: Vec2,
    ) -> MinimumNatural<Vec2> {
        self.widget.prefered_size_of_container(container)
    }
}

impl<T: Widget, F: Fn(Rect) -> Rect> Widget for LayoutRecord<T, F> {
    fn render(&self, buffer: &mut BufferSliceMut) {
        let rect = Rect {
            start: buffer.start(),
            size: buffer.size(),
        };
        self.record.set((self.mapping)(rect));
        self.widget.render(buffer);
    }
}
