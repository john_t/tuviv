//! Houses the [`Stack`] widget

use le::{layout::Vec2, Layout, MinimumNatural};

use crate::{buffer::BufferSliceMut, Widget, WidgetType};

/// A widget that stacks children on top
/// of each other.
#[derive(Default)]
pub struct Stack<'a> {
    /// The children to stack
    pub children: Vec<WidgetType<'a>>,
}

impl<'a> Stack<'a> {
    /// Creates a new [`Stack`]
    pub fn new() -> Self {
        Default::default()
    }

    /// Adds a child to this
    pub fn child<T: Widget + 'a>(mut self, child: T) -> Self {
        self.children.push(Box::new(child));
        self
    }
}

impl<'a> Layout for Stack<'a> {
    fn width_for_height(&self, height: usize) -> MinimumNatural<usize> {
        MinimumNatural {
            minimum: self
                .children
                .iter()
                .map(|x| x.width_for_height(height).minimum)
                .max()
                .unwrap_or(0),
            natural: self
                .children
                .iter()
                .map(|x| x.width_for_height(height).natural)
                .max()
                .unwrap_or(0),
        }
    }

    fn height_for_width(&self, width: usize) -> MinimumNatural<usize> {
        MinimumNatural {
            minimum: self
                .children
                .iter()
                .map(|x| x.height_for_width(width).minimum)
                .max()
                .unwrap_or(0),
            natural: self
                .children
                .iter()
                .map(|x| x.height_for_width(width).natural)
                .max()
                .unwrap_or(0),
        }
    }

    fn prefered_size(&self) -> MinimumNatural<Vec2> {
        let mut x_min = 0;
        let mut y_min = 0;
        let mut x_nat = 0;
        let mut y_nat = 0;
        for child in &self.children {
            let size = child.prefered_size();
            x_min = x_min.max(size.minimum.x);
            y_min = y_min.max(size.minimum.y);
            x_nat = x_nat.max(size.natural.x);
            y_nat = y_nat.max(size.natural.y);
        }
        MinimumNatural {
            minimum: Vec2::new(x_min, y_min),
            natural: Vec2::new(x_nat, y_nat),
        }
    }

    fn prefered_size_of_container(
        &self,
        container: Vec2,
    ) -> MinimumNatural<Vec2> {
        let mut x_min = 0;
        let mut y_min = 0;
        let mut x_nat = 0;
        let mut y_nat = 0;
        for child in &self.children {
            let size = child.prefered_size_of_container(container);
            x_min = x_min.max(size.minimum.x);
            y_min = y_min.max(size.minimum.y);
            x_nat = x_nat.max(size.natural.x);
            y_nat = y_nat.max(size.natural.y);
        }
        MinimumNatural {
            minimum: Vec2::new(x_min, y_min),
            natural: Vec2::new(x_nat, y_nat),
        }
    }
}

impl<'a> Widget for Stack<'a> {
    fn render(&self, buffer: &mut BufferSliceMut) {
        for child in &self.children {
            child.render(buffer);
        }
    }
}
