//! Houses the [`Ratio`] widget

use le::{Layout, MinimumNatural};

use crate::{
    buffer::BufferSliceMut,
    le::layout::{Rect, Vec2},
    Widget,
};

/// Renders a child to a given ratio
pub struct Ratio<T: Widget> {
    /// The child widget
    pub widget: T,
    /// The ratio (x ÷ y)
    pub ratio: f64,
}

impl<T: Widget> Ratio<T> {
    /// Creates a new `Ratio`
    ///
    /// See also [`WidgetExt::ratio`](crate::prelude::WidgetExt::to_ratio)
    pub fn new(widget: T, ratio: f64) -> Self {
        Self { widget, ratio }
    }
}

impl<T: Widget> Layout for Ratio<T> {
    fn width_for_height(&self, height: usize) -> MinimumNatural<usize> {
        MinimumNatural::same((height as f64 * self.ratio).round() as usize)
    }

    fn height_for_width(&self, width: usize) -> MinimumNatural<usize> {
        MinimumNatural::same((width as f64 / self.ratio).round() as usize)
    }

    fn prefered_size(&self) -> MinimumNatural<Vec2> {
        MinimumNatural {
            minimum: ratioify_size(
                self.widget.prefered_size().minimum,
                self.ratio,
                true,
            ),
            natural: ratioify_size(
                self.widget.prefered_size().natural,
                self.ratio,
                true,
            ),
        }
    }

    fn prefered_size_of_container(
        &self,
        container: Vec2,
    ) -> MinimumNatural<Vec2> {
        MinimumNatural {
            minimum: ratioify_size(
                self.widget
                    .prefered_size_of_container(ratioify_size(
                        container, self.ratio, false,
                    ))
                    .minimum,
                self.ratio,
                true,
            ),
            natural: ratioify_size(
                self.widget
                    .prefered_size_of_container(ratioify_size(
                        container, self.ratio, false,
                    ))
                    .natural,
                self.ratio,
                true,
            ),
        }
    }
}

/// Takes a size and puts it into the given ratio.
///
/// If expansive is true the new size will be bigger than the old,
/// whereas if it is false the new size will be smaller than old.
fn ratioify_size(mut size: Vec2, ratio: f64, expansive: bool) -> Vec2 {
    if (size.x < size.y) ^ !expansive {
        size.x = (size.y as f64 * ratio).round() as usize;
        size
    } else {
        size.y = (size.x as f64 / ratio).round() as usize;
        size
    }
}

impl<T: Widget> Widget for Ratio<T> {
    fn render(&self, buffer: &mut BufferSliceMut) {
        let size = ratioify_size(buffer.size(), self.ratio, false);
        let rect = Rect {
            start: Vec2::new(0, 0),
            size,
        };
        let mut buffer = buffer.sub_mut_slice(rect).unwrap();
        self.widget.render(&mut buffer);
    }
}
