use le::{
    align::Alignment,
    flexbox::{FlexLayout, Props},
    layout::Vec2,
    Layout, MinimumNatural,
};

use crate::WidgetType;

/// The child of a flexbox
///
/// See [`Props`]. Note all props can bet set through builder functions
#[must_use = "childs do nothing if not attached to their parent"]
pub struct Child<'a> {
    /// The widget of this child
    pub widget: WidgetType<'a>,
    /// The flexbox properties on how this should be layed out
    pub props: Props,
}

impl<'a> Child<'a> {
    /// Creates a new [`Child`]

    /// See [`WidgetExt`](crate::prelude::WidgetExt::to_flex_child) for
    /// a builder method
    pub fn new(widget: WidgetType<'a>) -> Self {
        Self {
            widget,
            props: Props::new(),
        }
    }

    /// Sets the `expand` property `self.props`
    pub fn expand(mut self, expand: usize) -> Self {
        self.props.expand = expand;
        self
    }

    /// Sets the `align` property `self.props`
    pub fn align(mut self, align: Alignment) -> Self {
        self.props.align = align;
        self
    }
}

impl Layout for Child<'_> {
    fn width_for_height(&self, height: usize) -> MinimumNatural<usize> {
        self.widget.width_for_height(height)
    }
    fn height_for_width(&self, width: usize) -> MinimumNatural<usize> {
        self.widget.height_for_width(width)
    }

    fn prefered_size(&self) -> MinimumNatural<Vec2> {
        self.widget.prefered_size()
    }

    fn prefered_size_of_container(
        &self,
        container: Vec2,
    ) -> MinimumNatural<Vec2> {
        self.widget.prefered_size_of_container(container)
    }
}

impl FlexLayout for Child<'_> {
    fn props(&self) -> &Props {
        &self.props
    }
}
