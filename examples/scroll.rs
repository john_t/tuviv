use std::{
    convert::Infallible,
    io::{self},
};

use crossterm::event::{Event, KeyCode};
use le::{layout::Vec2, Orientation};
use tuviv::{
    prelude::*,
    widgets::{Filler, Flexbox},
    Color,
};

static LOREM_IPSUM: &str = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. ";
const N_ROWS: u32 = 16;

fn main() -> io::Result<()> {
    let mut scroll = Vec2::<usize>::new(0, 0);

    tuviv::run_app(move |event| {
        if let Event::Key(key) = event {
            match key.code {
                KeyCode::Char('j') => {
                    scroll.y += 1;
                }
                KeyCode::Char('k') => {
                    scroll.y = scroll.y.saturating_sub(1);
                }
                KeyCode::Char('q') => {
                    return Ok(None);
                }
                _ => (),
            }
        }

        let mut f = Flexbox::new(Orientation::Vertical, false);
        for i in 0..N_ROWS {
            let v = i * 255 / N_ROWS;
            let v = v as u8;
            f.children
                .push(Filler::new(" ".bg(Color::Rgb(v, v, 0))).to_flex_child())
        }

        let p = f
            .to_scroll()
            .scroll_x(false)
            .scroll_y(true)
            .position(scroll);

        Ok::<_, Infallible>(Some(p))
    })?;

    Ok(())
}
