use le::{
    grid::{layout_grid, GridConfig, Layouted, Sizing},
    layout::Vec2,
    Layout, MinimumNatural,
};

use super::Child;
use crate::{border::Border, buffer::BufferSliceMut, prelude::*, Rect, Widget};

/// A widget which arranges its children in a grid
#[derive(Default)]
pub struct Grid<'a> {
    /// The children of the grid
    pub children: Vec<Child<'a>>,
    /// The template rows.
    ///
    /// See the equivilent [css](https://www.w3schools.com/css/css_grid_container.asp)
    pub template_rows: Vec<Sizing>,
    /// The template columns.
    ///
    /// See the equivilent [css](https://www.w3schools.com/css/css_grid_container.asp)
    pub template_columns: Vec<Sizing>,
    /// The gap between the rows
    pub row_gap: usize,
    /// The gap between the columns
    pub column_gap: usize,
    /// Draws a border around the grid.
    ///
    /// It will only draw between cells if `row_gap` or
    /// `column_gap` are non-zero
    pub border: Border,
    /// Which position to automatically place children in.
    auto_pos: Vec2,
}

impl<'a> Grid<'a> {
    /// Creates a new [`Grid`]
    #[must_use]
    pub fn new() -> Self {
        Default::default()
    }

    /// Places child automatically, going row by row
    pub fn auto_child<T: Widget + 'a>(mut self, child: T) -> Self {
        let child = child.to_grid_child();
        self.children.push(
            child
                .column_span(1)
                .row_span(1)
                .column(self.auto_pos.x)
                .row(self.auto_pos.y),
        );
        self.auto_pos.x += 1;
        if self.auto_pos.x >= self.template_columns.len() {
            self.auto_pos.y += 1;
            self.auto_pos.x = 0;
        }
        self
    }

    /// Gets the layout information for this grid.
    pub fn layout(
        &self,
        rect: Rect,
        fill_space: bool,
        minimum: bool,
    ) -> Layouted {
        let grid_config = GridConfig {
            template_rows: &self.template_rows,
            template_columns: &self.template_columns,
            row_gap: self.row_gap,
            column_gap: self.column_gap,
            fill_space,
            minimum,
        };
        layout_grid(rect, grid_config, &self.children)
    }

    /// Adds a child to the grid which already
    /// has layout information
    pub fn child(mut self, child: Child<'a>) -> Self {
        self.children.push(child);
        self
    }

    /// Sets the [`Grid::template_rows`] property
    pub fn template_rows(mut self, template_rows: Vec<Sizing>) -> Self {
        self.template_rows = template_rows;
        self
    }

    /// Sets the [`Grid::template_columns`] property
    pub fn template_columns(mut self, template_columns: Vec<Sizing>) -> Self {
        self.template_columns = template_columns;
        self
    }

    /// Adds a template row
    pub fn template_row(mut self, row: Sizing) -> Self {
        self.template_rows.push(row);
        self
    }

    /// Adds a template column
    pub fn template_column(mut self, column: Sizing) -> Self {
        self.template_columns.push(column);
        self
    }

    /// Sets the [`Grid::row_gap`] property
    pub fn row_gap(mut self, row_gap: usize) -> Self {
        self.row_gap = row_gap;
        self
    }

    /// Sets the [`Grid::column_gap`] property
    pub fn column_gap(mut self, column_gap: usize) -> Self {
        self.column_gap = column_gap;
        self
    }

    /// Sets the [`Grid::border`] property
    pub fn border(mut self, border: Border) -> Self {
        self.border = border;
        self
    }
}

impl Layout for Grid<'_> {
    fn width_for_height(&self, height: usize) -> MinimumNatural<usize> {
        let size =
            self.prefered_size_of_container(Vec2::new(usize::MAX / 2, height));
        let border_space = self.border.border().extra().x;

        MinimumNatural {
            minimum: size.minimum.x + border_space,
            natural: size.natural.x + border_space,
        }
    }

    fn height_for_width(&self, width: usize) -> MinimumNatural<usize> {
        let size =
            self.prefered_size_of_container(Vec2::new(width, usize::MAX / 2));
        let border_space = self.border.border().extra().y;

        MinimumNatural {
            minimum: size.minimum.y + border_space,
            natural: size.natural.y + border_space,
        }
    }

    fn prefered_size(&self) -> MinimumNatural<Vec2> {
        self.prefered_size_of_container(Vec2::new(
            usize::MAX / 2,
            usize::MAX / 2,
        ))
    }

    fn prefered_size_of_container(
        &self,
        container: Vec2,
    ) -> MinimumNatural<Vec2> {
        let rect = Rect {
            start: Vec2::new(0, 0),
            size: container,
        };
        let border_space = self.border.border().extra();
        MinimumNatural {
            minimum: size_from_layout(self.layout(rect, false, true))
                + border_space,
            natural: size_from_layout(self.layout(rect, false, false))
                + border_space,
        }
    }
}

impl Widget for Grid<'_> {
    fn render(&self, buffer: &mut BufferSliceMut<'_>) {
        let border_extra = self.border.border();
        let grid_rect = Rect {
            start: Vec2::new(border_extra.top, border_extra.left),
            size: buffer.size().saturating_sub(border_extra.extra()),
        };

        let layout = self.layout(grid_rect, true, false);

        self.border.draw_border(buffer);

        let column_gap_center = self.column_gap / 2;
        let row_gap_center = self.row_gap / 2;

        let mut column_total = 0_usize;

        // Draw the vertical columns separators
        let start_y = 1;
        let end_y = buffer.size().y.saturating_sub(1);

        // Loop through each column
        let mut columns = Vec::new();
        for column in &layout.columns[0..(layout.columns.len() - 1)] {
            column_total += column + self.column_gap;
            columns.push(column_total);

            // Draw the column separators
            if let Some(up_down) = self.border.up_down {
                for y in start_y..end_y {
                    buffer.write(
                        Vec2::new(column_total - column_gap_center, y),
                        up_down.to_string().with_style(self.border.style),
                    );
                }
            }

            // Draw the joiner at the top
            if let Some(left_right_down) = self.border.left_right_down {
                buffer.write(
                    Vec2::new(column_total - column_gap_center, start_y - 1),
                    left_right_down.to_string().with_style(self.border.style),
                );
            }

            // Draw the joiner at the bottom
            if let Some(left_right_up) = self.border.left_right_up {
                buffer.write(
                    Vec2::new(column_total - column_gap_center, end_y),
                    left_right_up.to_string().with_style(self.border.style),
                );
            }
        }

        let start_x = 1;
        let end_x = buffer.size().x.saturating_sub(1);
        let mut row_total = 0_usize;

        // Loop through each row
        for row in &layout.rows[0..(layout.rows.len() - 1)] {
            row_total += row + self.row_gap;

            // Draw the row separators
            if let Some(left_right) = self.border.left_right {
                for x in start_x..end_x {
                    buffer.write(
                        Vec2::new(x, row_total - row_gap_center),
                        left_right.to_string().with_style(self.border.style),
                    );
                }
            }

            // Draw the joiner at the left
            if let Some(up_down_right) = self.border.up_down_right {
                buffer.write(
                    Vec2::new(0, row_total - row_gap_center),
                    up_down_right.to_string().with_style(self.border.style),
                );
            }

            // Draw the joiner at the right
            if let Some(up_down_left) = self.border.up_down_left {
                buffer.write(
                    Vec2::new(
                        buffer.size().x.saturating_sub(1),
                        row_total - row_gap_center,
                    ),
                    up_down_left.to_string().with_style(self.border.style),
                );
            }

            // Draw the vertical-horizontal joiners
            if let Some(left_right_up_down) = self.border.left_right_up_down {
                for column in &columns {
                    buffer.write(
                        Vec2::new(
                            column - column_gap_center,
                            row_total - row_gap_center,
                        ),
                        left_right_up_down
                            .to_string()
                            .with_style(self.border.style),
                    );
                }
            }
        }

        for (child, rect) in self.children.iter().zip(layout.sizes.into_iter())
        {
            child
                .widget
                .render(&mut buffer.sub_mut_slice(rect).unwrap());
        }
    }
}

/// Works out the size from a layout by finding the max x/ys
fn size_from_layout(layout: Layouted) -> Vec2 {
    Vec2::new(
        layout.sizes.iter().map(|x| x.end().x).max().unwrap_or(0),
        layout.sizes.iter().map(|y| y.end().y).max().unwrap_or(0),
    )
}
