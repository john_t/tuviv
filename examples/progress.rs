mod mouse;

use std::io::{self};
use std::time::Duration;

use le::{align::Alignment, grid::Sizing};
use tuviv::{
    border::Border,
    prelude::*,
    widgets::{Grid, Paragraph, ProgressBar},
};

fn main() -> io::Result<()> {
    let grid = Grid::new()
        .template_rows(vec![
            Sizing::Auto,
            Sizing::Auto,
            Sizing::Auto,
            Sizing::Fixed(6),
        ])
        .template_columns(vec![Sizing::Auto, Sizing::AutoFixed(10)])
        .column_gap(2)
        .auto_child(Paragraph::new("CPU".styled()))
        .auto_child(
            ProgressBar::<1>::new()
                .total(100.0)
                .value(10.0)
                .vertical()
                .align_y(Alignment::Center),
        )
        .auto_child(Paragraph::new("MEM".styled()))
        .auto_child(
            ProgressBar::<1>::new()
                .total(8.0)
                .value(2.4)
                .align_y(Alignment::Center),
        )
        .auto_child(Paragraph::new("GPU".styled()))
        .auto_child(
            ProgressBar::new()
                .total(15.0)
                .value(8.0)
                .fg(vec!["─".styled().bold()])
                .edge(["C".styled().yellow().bold()])
                .bg(vec!["O o ".styled()])
                .align_y(Alignment::Center),
        )
        .auto_child(Paragraph::new("Verticality".styled()))
        .auto_child(
            ProgressBar::new()
                .total(100.0)
                .value(50.0)
                .vertical()
                .bg(vec!["│".styled().bright_blue()])
                .edge(["▄".styled().green().reversed()])
                .align_x(Alignment::Center),
        )
        .to_box_sizing()
        .border(Border::ROUNDED)
        .centered();

    tuviv::render_frame(&grid, Duration::from_secs(2))?;

    Ok(())
}
