use either::{Either, Left, Right};

use crate::{buffer::BufferSliceMut, Widget};

impl<L: Widget, R: Widget> Widget for Either<L, R> {
    fn render(&self, buffer: &mut BufferSliceMut<'_>) {
        match self {
            Left(x) => x.render(buffer),
            Right(x) => x.render(buffer),
        }
    }
}
