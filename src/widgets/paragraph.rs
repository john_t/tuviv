//! Houses the [`Paragraph`] widget

use std::{borrow::Cow, iter};

use le::{
    layout::{LayoutInfo, Vec2},
    Alignment, Layout, MinimumNatural, Orientation,
};
use unicode_width::UnicodeWidthStr;

use crate::{
    buffer::BufferSliceMut, prelude::*, widgets::Flexbox, StyledText, Widget,
};

/// A paragraph which can have multiple multicoloured-texts
#[derive(Clone)]
pub struct Paragraph<'a> {
    /// The text to display
    pub text: Vec<StyledText<'a>>,
    /// How to horizontally align the text
    /// (for vertical alignment use
    /// [`align_y`](crate::prelude::WidgetExt::align_y)
    pub text_align: Alignment,
    /// The prefered ratio x/y of the paragraph
    ///
    /// Remember, in monospaced fonts there is normally a ratio
    /// of 1 y to every 2 x
    ///
    /// The default is `3.0`
    pub prefered_ratio: f64,
    /// Wrap the paragraph?
    pub wrap: bool,
}

impl<'a> Paragraph<'a> {
    /// Creates a new paragraph with the text
    pub fn new(text: StyledText<'a>) -> Self {
        Self::new_texts(vec![text])
    }

    /// Creates a paragraph that doesn't wrap
    pub fn label(text: StyledText<'a>) -> Self {
        Self::new_texts(vec![text]).wrap(false)
    }

    /// Creates a new paragraph with multiple spans
    pub fn new_texts(text: Vec<StyledText<'a>>) -> Self {
        Self {
            text,
            text_align: Alignment::Begin,
            prefered_ratio: 3.0,
            wrap: true,
        }
    }

    /// Gets the maximum possible width of the paragraph
    pub fn text_max_width(&self) -> usize {
        self.text.iter().map(|x| x.text.width()).sum()
    }

    /// Sets the [`Paragraph::text_align`] property.
    pub fn text_align(mut self, text_align: Alignment) -> Self {
        self.text_align = text_align;
        self
    }

    /// Sets the [`Paragraph::wrap`] property.
    pub fn wrap(mut self, wrap: bool) -> Self {
        self.wrap = wrap;
        self
    }

    /// Sets the [`Paragraph::prefered_ratio`] property.
    pub fn prefered_ratio(mut self, prefered_ratio: f64) -> Self {
        self.prefered_ratio = prefered_ratio;
        self
    }

    /// Creates multiple paragraphs from this, mainly useful
    /// if newlines are used with wrapping
    pub fn create_paragraphs(&self) -> impl Widget {
        let mut flexbox = Flexbox::new(Orientation::Vertical, false);
        let mut accumulation = Vec::new();
        for span in &self.text {
            let mut prev_pos = 0;
            while let Some(mut pos) = span.text[prev_pos..].find('\n') {
                pos += prev_pos;
                if prev_pos != pos {
                    accumulation.push(StyledText {
                        text: Cow::Owned(
                            span.text[prev_pos..pos].trim_end().to_string(),
                        ),
                        style: span.style,
                    });
                    flexbox.children.push(
                        Paragraph {
                            text: accumulation,
                            text_align: self.text_align,
                            // TODO: Not this, needs a special widget
                            // TODO: some sort of 'paragraph factory'
                            prefered_ratio: self.prefered_ratio,
                            wrap: self.wrap,
                        }
                        .to_flex_child(),
                    );
                }
                accumulation = Vec::new();
                prev_pos = pos;
                prev_pos += 1;
                while !span.text.is_char_boundary(prev_pos) {
                    prev_pos += 1;
                    if prev_pos >= span.text.len() {
                        break;
                    }
                }
            }
            accumulation.push(StyledText {
                text: Cow::Owned(span.text[prev_pos..].to_string()),
                style: span.style,
            });
        }
        flexbox.children.push(
            Paragraph {
                text: accumulation,
                text_align: self.text_align,
                prefered_ratio: self.prefered_ratio,
                wrap: self.wrap,
            }
            .to_flex_child(),
        );
        flexbox
    }

    /// Gets the ratio of the text at a given width.
    fn width_ratio(
        &self,
        width: usize,
        splitted: &'a [StyledText<'a>],
    ) -> Option<(f64, usize)> {
        // Wrap the text at the given amount.
        let wrapped = wrap_first_fit(splitted, width);
        let wrapped_len = wrapped.count();

        let mut ratio =
            (width as f64 / self.prefered_ratio) / wrapped_len as f64;
        if ratio < self.prefered_ratio {
            ratio = self.prefered_ratio / ratio;
        }

        Some((ratio, wrapped_len))
    }
}

impl Layout for Paragraph<'_> {
    fn width_for_height(&self, _height: usize) -> MinimumNatural<usize> {
        MinimumNatural::same(if self.wrap {
            let splitted = split_words(&self.text);
            let min_x =
                splitted.iter().map(|x| x.text.width()).max().unwrap_or(0);
            let max_x = self.text.iter().map(|x| x.text.width()).sum();
            let x: usize = (min_x..=max_x)
                .map(|x| (x, wrap_first_fit(&splitted, x).count()))
                .reduce(
                    |(x1, n1), (x2, n2)| {
                        if n1 > n2 {
                            (x1, n1)
                        } else {
                            (x2, n2)
                        }
                    },
                )
                .map(|(x, _n)| x)
                .unwrap_or_default();
            x
        } else {
            self.text_max_width()
        })
    }

    fn height_for_width(&self, width: usize) -> MinimumNatural<usize> {
        let height = if self.wrap {
            let splitted = split_words(&self.text);
            wrap_first_fit(&splitted, width).count()
        } else {
            1
        };
        MinimumNatural::same(height)
    }

    fn prefered_size(&self) -> MinimumNatural<Vec2> {
        if !self.wrap {
            return MinimumNatural::same(Vec2::new(self.text_max_width(), 1));
        }

        let splitted = split_words(&self.text);
        let min_x = splitted.iter().map(|x| x.text.width()).max().unwrap_or(0);
        let max_x = self.text.iter().map(|x| x.text.width()).sum();
        let (x, (_, y)) = (min_x..=max_x)
            .map(|x| {
                (
                    x,
                    self.width_ratio(x, &splitted)
                        .unwrap_or((f64::INFINITY, 0)),
                )
            })
            .reduce(|(x1, (ratio1, y1)), (x2, (ratio2, y2))| {
                if ratio1 > ratio2 {
                    (x2, (ratio2, y2))
                } else {
                    (x1, (ratio1, y1))
                }
            })
            .unwrap_or_default();
        MinimumNatural::same(Vec2::new(x, y))
    }

    fn prefered_size_of_container(
        &self,
        container: Vec2,
    ) -> MinimumNatural<Vec2> {
        let pref_size = self.prefered_size();

        if pref_size.natural.x < container.x {
            pref_size
        } else {
            MinimumNatural::same(Vec2::new(
                container.x,
                self.height_for_width(container.x).natural,
            ))
        }
    }
}

/// Returns the indicies in which the text must be wrapped
fn wrap_first_fit<'a>(
    text: &'a [StyledText<'_>],
    line_width: usize,
) -> impl Iterator<Item = usize> + 'a {
    text.iter()
        .enumerate()
        .scan(0_usize, move |accum, (i, text)| {
            let width = text.text.width();
            if *accum + width > line_width {
                *accum = width;
                Some((true, i))
            } else {
                *accum += width;
                Some((false, i))
            }
        })
        .filter_map(|(wrapped, word)| if wrapped { Some(word) } else { None })
        .chain(iter::once(text.len()))
}

impl Widget for Paragraph<'_> {
    fn render(&self, buffer: &mut BufferSliceMut<'_>) {
        let li = LayoutInfo {
            start: 0,
            end: buffer.size().x,
        };

        if self.wrap {
            // Wrap the text
            let splitted = split_words(&self.text);
            let wrapped = wrap_first_fit(&splitted, buffer.size().x);

            // Write it line by line
            let mut start = 0_usize;
            for (y, line) in wrapped.enumerate() {
                let texts = &splitted[start..line];
                let aligned = self
                    .text_align
                    .align(li, texts.iter().map(|x| x.text.width()).sum());
                let mut x_pos = aligned.start;

                for span in texts {
                    buffer.write_span(Vec2::new(x_pos, y), span);
                    x_pos += span.text.width();
                }
                start = line;
            }
        } else {
            let splitted = split_words(&self.text);
            let aligned = self.text_align.align(li, self.text_max_width());
            let mut x_pos = aligned.start;

            for span in splitted {
                buffer.write_span(Vec2::new(x_pos, 0), &span);
                x_pos += span.text.width();
            }
        }
    }
}

/// Splits words based of whitespace into many spans
///
/// This does not allocate more strings
fn split_words<'b: 'a, 'a>(text: &'b [StyledText<'a>]) -> Vec<StyledText<'a>> {
    // Splits based of whitespace, generally what is wanted.
    text.iter()
        .flat_map(|x| {
            x.text
                .split_inclusive(char::is_whitespace)
                .map(|text| StyledText {
                    text: Cow::Borrowed(text),
                    style: x.style,
                })
        })
        .collect()
}
