#[cfg(not(feature = "image"))]
compile_error!("This example needs the `image` feature to be enabled");

use std::io;
use std::time::Duration;

use image::io::Reader as ImageReader;
use tuviv::{prelude::*, widgets::Image};

fn main() -> io::Result<()> {
    let img = ImageReader::open("examples/image.jpg")?.decode().unwrap();

    let img = Image::new(img).centered();

    tuviv::render_frame(&img, Duration::from_secs(2))?;

    Ok(())
}
