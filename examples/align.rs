use std::io;
use std::time::Duration;

use le::Orientation;
use tuviv::{
    prelude::*,
    widgets::{Filler, Flexbox},
};

fn main() -> io::Result<()> {
    let flexbox = Flexbox::new(Orientation::Horizontal, false)
        .child(
            Filler::new(" ".styled().bg_red())
                .fixed_size(16, 8)
                .centered()
                .to_flex_child()
                .expand(1),
        )
        .child(
            Flexbox::new(Orientation::Vertical, false)
                .child(
                    Filler::new(" ".styled().bg_yellow())
                        .fixed_size(16, 8)
                        .centered()
                        .to_flex_child()
                        .expand(1),
                )
                .child(
                    Filler::new(" ".styled().bg_green())
                        .fixed_size(16, 8)
                        .centered()
                        .to_flex_child()
                        .expand(1),
                )
                .to_flex_child()
                .expand(1),
        );

    tuviv::render_frame(&flexbox, Duration::from_secs(2))?;

    Ok(())
}
