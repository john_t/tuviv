//! Terminal backends

#[cfg(feature = "crossterm")]
mod crossterm;
#[cfg(feature = "crossterm")]
pub use self::crossterm::CrosstermBackend;

use std::io::{self, Write};

use super::buffer::BufferSlice;

/// A trait to represent a different terminal backend such as
/// crossterm.
pub trait Backend {
    /// Writes the buffer to the writer through the backend
    ///
    /// # Errors
    ///
    /// Errors in case of usual IO errors
    fn finish(
        &self,
        buffer: &BufferSlice<'_>,
        prev_buffer: &BufferSlice<'_>,
        writer: &mut impl Write,
    ) -> io::Result<()>;
}
