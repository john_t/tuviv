use le::{
    grid::{GridLayout, Props},
    layout::Vec2,
    Layout, MinimumNatural,
};

use crate::WidgetType;

/// The child of a grid element
#[must_use = "childs do nothing if not attached to their parent"]
pub struct Child<'a> {
    /// The widget to render
    pub widget: WidgetType<'a>,
    /// The layout properties
    pub props: Props,
}

impl<'a> Child<'a> {
    /// Creates a new [`Child`]
    ///
    /// See [`WidgetExt`](crate::prelude::WidgetExt::to_grid_child) for
    /// a builder method
    pub fn new(widget: WidgetType<'a>, props: Props) -> Self {
        Self { widget, props }
    }

    /// Sets the `row` property on `self.props`
    pub fn row(mut self, row: usize) -> Self {
        self.props.row = row;
        self
    }

    /// Sets the `column` property on `self.props`
    pub fn column(mut self, column: usize) -> Self {
        self.props.column = column;
        self
    }

    /// Sets the `row_span` property on `self.props`
    pub fn row_span(mut self, row_span: usize) -> Self {
        self.props.row_span = row_span;
        self
    }

    /// Sets the `column_span` property on `self.props`
    pub fn column_span(mut self, column_span: usize) -> Self {
        self.props.column_span = column_span;
        self
    }
}

impl Layout for Child<'_> {
    fn width_for_height(&self, height: usize) -> MinimumNatural<usize> {
        self.widget.width_for_height(height)
    }
    fn height_for_width(&self, width: usize) -> MinimumNatural<usize> {
        self.widget.height_for_width(width)
    }

    fn prefered_size(&self) -> MinimumNatural<Vec2> {
        self.widget.prefered_size()
    }

    fn prefered_size_of_container(
        &self,
        container: Vec2,
    ) -> MinimumNatural<Vec2> {
        self.widget.prefered_size_of_container(container)
    }
}

impl GridLayout for Child<'_> {
    fn props(&self) -> &Props {
        &self.props
    }
}
