use le::{
    flexbox::layout_flexbox,
    layout::{Rect, Vec2},
    Layout, MinimumNatural, Orientation,
};

use super::Child;
use crate::{buffer::BufferSliceMut, Widget};

/// A css inspired flexbox layout widget.
pub struct Flexbox<'a> {
    /// The children (in order) of the flexbox.
    pub children: Vec<Child<'a>>,
    /// The orientation of the flexbox (column/row)
    pub orientation: Orientation,
    /// The gap between each row
    pub row_gap: usize,
    /// The gap between each column
    pub column_gap: usize,
    /// Should the flexbox wrap if there is not enough space?
    pub wrap: bool,
}

impl<'a> Flexbox<'a> {
    /// Creates a new flexbox
    #[must_use]
    pub fn new(orientation: Orientation, wrap: bool) -> Self {
        Self {
            children: Vec::new(),
            orientation,
            row_gap: 0,
            column_gap: 0,
            wrap,
        }
    }

    /// Returns the layout information for the flexbox.
    pub fn layout(&self, rect: Rect, expand: bool, minimum: bool) -> Vec<Rect> {
        layout_flexbox(
            rect,
            self.wrap,
            minimum,
            expand,
            self.row_gap,
            self.column_gap,
            self.orientation,
            &self.children,
        )
    }

    /// Adds a child to the flexbox
    pub fn child(mut self, child: Child<'a>) -> Self {
        self.children.push(child);
        self
    }

    /// Sets the [`Flexbox::row_gap`] property
    pub fn row_gap(mut self, row_gap: usize) -> Self {
        self.row_gap = row_gap;
        self
    }

    /// Sets the [`Flexbox::column_gap`] property
    pub fn column_gap(mut self, column_gap: usize) -> Self {
        self.column_gap = column_gap;
        self
    }

    /// Sets the [`Flexbox::wrap`] property
    pub fn wrap(mut self, wrap: bool) -> Self {
        self.wrap = wrap;
        self
    }
}

impl Layout for Flexbox<'_> {
    fn width_for_height(&self, height: usize) -> MinimumNatural<usize> {
        match self.orientation {
            Orientation::Vertical => MinimumNatural {
                minimum: self
                    .children
                    .iter()
                    .map(|x| x.widget.width_for_height(height).minimum)
                    .max()
                    .unwrap_or(0),
                natural: self
                    .children
                    .iter()
                    .map(|x| x.widget.width_for_height(height).natural)
                    .max()
                    .unwrap_or(0),
            },
            Orientation::Horizontal => {
                let gap =
                    self.children.len().saturating_sub(1) * self.column_gap;
                MinimumNatural {
                    minimum: self
                        .children
                        .iter()
                        .map(|x| x.widget.width_for_height(height).minimum)
                        .sum::<usize>()
                        + gap,
                    natural: self
                        .children
                        .iter()
                        .map(|x| x.widget.width_for_height(height).natural)
                        .sum::<usize>()
                        + gap,
                }
            }
        }
    }

    fn height_for_width(&self, width: usize) -> MinimumNatural<usize> {
        match self.orientation {
            Orientation::Vertical => {
                let gap = self.children.len().saturating_sub(1) * self.row_gap;
                MinimumNatural {
                    minimum: self
                        .children
                        .iter()
                        .map(|x| x.widget.height_for_width(width).minimum)
                        .sum::<usize>()
                        + gap,
                    natural: self
                        .children
                        .iter()
                        .map(|x| x.widget.height_for_width(width).natural)
                        .sum::<usize>()
                        + gap,
                }
            }
            Orientation::Horizontal => {
                if self.wrap {
                    MinimumNatural {
                        minimum: {
                            let layout = self.layout(
                                Rect::new(0, 0, width, 1),
                                false,
                                true,
                            );
                            layout
                                .iter()
                                .zip(self.children.iter())
                                .map(|(layout, widget)| {
                                    widget
                                        .height_for_width(layout.end().x)
                                        .minimum
                                })
                                .max()
                                .unwrap_or(0)
                        },
                        natural: {
                            let layout = self.layout(
                                Rect::new(0, 0, width, 1),
                                false,
                                false,
                            );
                            layout
                                .iter()
                                .zip(self.children.iter())
                                .map(|(layout, widget)| {
                                    widget
                                        .height_for_width(layout.end().x)
                                        .natural
                                })
                                .max()
                                .unwrap_or(0)
                        },
                    }
                } else {
                    MinimumNatural {
                        minimum: self
                            .children
                            .iter()
                            .map(|x| x.widget.height_for_width(width).minimum)
                            .max()
                            .unwrap_or(0),
                        natural: self
                            .children
                            .iter()
                            .map(|x| x.widget.height_for_width(width).natural)
                            .max()
                            .unwrap_or(0),
                    }
                }
            }
        }
    }

    fn prefered_size(&self) -> MinimumNatural<Vec2> {
        let max_rect = Rect::new(0, 0, usize::MAX / 2, usize::MAX / 2);
        MinimumNatural {
            minimum: {
                let layout = self.layout(max_rect, false, true);
                size_from_rects(&layout)
            },
            natural: {
                let layout = self.layout(max_rect, false, false);
                size_from_rects(&layout)
            },
        }
    }

    fn prefered_size_of_container(
        &self,
        container: Vec2,
    ) -> MinimumNatural<Vec2> {
        let rect = Rect {
            start: Vec2::new(0, 0),
            size: container,
        };
        MinimumNatural {
            minimum: {
                let layout = self.layout(rect, false, true);
                let size = size_from_rects(&layout);
                Vec2::new(size.x.min(container.x), size.y.min(container.y))
            },
            natural: {
                let layout = self.layout(rect, false, false);
                let size = size_from_rects(&layout);
                Vec2::new(size.x.min(container.x), size.y.min(container.y))
            },
        }
    }
}

impl Widget for Flexbox<'_> {
    fn render(&self, buffer: &mut BufferSliceMut<'_>) {
        let rect = Rect {
            start: Vec2::new(0, 0),
            size: buffer.size(),
        };
        let layout = self.layout(rect, true, false);
        for (child, rect) in self.children.iter().zip(layout.into_iter()) {
            // eprintln!("buffer: {}x{}", buffer.size().x, buffer.size().y);
            // eprintln!(
            //     "rect: {}x{}+{}+{}",
            //     rect.size.x, rect.size.y, rect.start.x, rect.start.y
            // );
            // if rect.start < buffer.size() {
            //     rect.size = rect.size.min(buffer.size() - rect.start);
            // }
            if let Some(mut child_buffer) = buffer.sub_mut_slice_clipped(rect) {
                child.widget.render(&mut child_buffer);
            }
        }
    }
}

/// Gets the size of a flexbox from the layouted rects
fn size_from_rects(layout: &[Rect]) -> Vec2 {
    let mut width = 0;
    let mut height = 0;

    for item in layout {
        width = width.max(item.end().x);
        height = height.max(item.end().y);
    }

    Vec2::new(width, height)
}
