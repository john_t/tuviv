use std::io::{self};
use std::time::Duration;

use le::Orientation;
use tuviv::{
    prelude::*,
    widgets::{Filler, Flexbox},
    Color,
};

fn main() -> io::Result<()> {
    let flexbox = Flexbox::new(Orientation::Horizontal, false)
        .column_gap(2)
        .child(Filler::new(" ".bg(Color::Red)).to_flex_child().expand(1))
        .child(
            Flexbox::new(Orientation::Vertical, false)
                .row_gap(1)
                .child(
                    Filler::new(" ".bg(Color::Green)).to_flex_child().expand(1),
                )
                .child(
                    Filler::new(" ".bg(Color::Yellow))
                        .to_flex_child()
                        .expand(1),
                )
                .to_flex_child()
                .expand(1),
        );

    tuviv::render_frame(&flexbox, Duration::from_secs(2))?;

    Ok(())
}
