use std::{
    cell::Cell,
    convert::Infallible,
    io::{self},
    rc::Rc,
};

use crossterm::event::{Event, KeyCode, MouseButton, MouseEventKind};
use le::{
    layout::{Rect, Vec2, TRBL}, Orientation,
};
use tuviv::{
    border::Border,
    prelude::*,
    widgets::{Char, Flexbox, Paragraph},
    Color, Style,
};

const MOUSE_BUTTON: MouseButton = MouseButton::Left;
const PADDING: TRBL = TRBL::new(0, 1, 0, 1);

fn main() -> io::Result<()> {
    let scroll = Vec2::<usize>::new(0, 0);
    let mut mouse_pos = Vec2::new(0_usize, 0);
    let mut mouse_down = false;
    let mut value = 0_isize;
    let plus_pos = Rc::new(Cell::new(Rect::new(0, 0, 0, 0)));
    let minus_pos = Rc::new(Cell::new(Rect::new(0, 0, 0, 0)));

    tuviv::run_app(move |event| {
        match event {
            Event::Mouse(mouse) => {
                mouse_pos = Vec2::new(mouse.column.into(), mouse.row.into());
                match mouse.kind {
                    MouseEventKind::Down(b) => mouse_down = true,
                    _ => mouse_down = false,
                }
            }
            Event::Key(key) => {
                if let KeyCode::Char('q') = key.code {
                    return Ok(None);
                }
            }
            _ => (),
        }

        let plus_border_colour = if plus_pos.get().contains(mouse_pos) {
            if mouse_down {
                value += 1;
            }
            Color::Yellow
        } else {
            Color::Reset
        };

        let minus_border_colour = if minus_pos.get().contains(mouse_pos) {
            if mouse_down {
                value -= 1;
            }
            Color::Yellow
        } else {
            Color::Reset
        };

        let w = Flexbox::new(Orientation::Horizontal, true)
            .child(
                Paragraph::new(value.to_string().into())
                    .to_box_sizing()
                    .border(Border::MODERN)
                    .padding(PADDING)
                    .to_flex_child(),
            )
            .child(
                Char::new('+')
                    .to_box_sizing()
                    .border(
                        Border::MODERN
                            .styled(Style::default().fg(plus_border_colour)),
                    )
                    .padding(PADDING)
                    .to_layout_record(plus_pos.clone())
                    .to_flex_child(),
            )
            .child(
                Char::new('-')
                    .to_box_sizing()
                    .border(
                        Border::MODERN
                            .styled(Style::default().fg(minus_border_colour)),
                    )
                    .padding(PADDING)
                    .to_layout_record(minus_pos.clone())
                    .to_flex_child(),
            )
            .centered();

        Ok::<_, Infallible>(Some(w))
    })?;

    Ok(())
}
