use std::io::{self};
use std::time::Duration;

use le::{layout::TRBL, Alignment};
use tuviv::{border::Border, prelude::*, widgets::Paragraph};

fn main() -> io::Result<()> {
    let p = Paragraph::new_texts(vec![
        "This is some text, which can be styled in many ways. For example "
            .styled(),
        "red".styled().red(),
        ", ".styled(),
        "blue background,".styled().bg_blue(),
        " ".styled(),
        "bold".styled().bold(),
        ", ".styled(),
        "like a spelling mistake"
            .styled()
            .undercurled()
            .underline_colored_bright_red(),
        ", ".styled(),
        "etc...".styled(),
    ])
    .text_align(Alignment::Center)
    .to_box_sizing()
    .border(Border::MODERN)
    .padding(TRBL::new(1, 2, 1, 2))
    .centered();

    tuviv::render_frame(&p, Duration::from_secs(2))?;

    Ok(())
}
