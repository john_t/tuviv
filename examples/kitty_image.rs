#[cfg(not(feature = "kitty"))]
compile_error!("This example needs the `kitty` feature to be enabled");

use std::num::NonZeroU32;
use std::time::Duration;

use image::io::Reader as ImageReader;
use image::ImageResult;
use tuviv::{prelude::*, widgets::KittyImage};

fn main() -> ImageResult<()> {
    let img = ImageReader::open("examples/image.jpg")?.decode().unwrap();

    let img = KittyImage::new(&img, NonZeroU32::new(2).unwrap())?.centered();

    tuviv::render_frame(&img, Duration::from_secs(1))?;

    Ok(())
}
