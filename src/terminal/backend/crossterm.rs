//! Provides a method for using crossterm with tuviv

use std::io::{self, BufWriter, Write};

use ::crossterm::{
    cursor,
    style::{
        Attribute as CrossAttribute, Attributes as CrossAttributes,
        Color as CrossColor, Print, SetAttribute, SetAttributes,
        SetBackgroundColor, SetForegroundColor,
    },
    QueueableCommand,
};
use crossterm::style::SetUnderlineColor;

use le::layout::Vec2;

use crate::{buffer::BufferSlice, Backend};

/// A backend which uses crossterm.
pub struct CrosstermBackend;

impl Backend for CrosstermBackend {
    fn finish(
        &self,
        buffer: &BufferSlice<'_>,
        prev_buffer: &BufferSlice<'_>,
        writer: &mut impl Write,
    ) -> io::Result<()> {
        let mut writer = BufWriter::new(writer);
        let buffers_size_eq = buffer.size() == prev_buffer.size();

        for y in 0..buffer.size().y {
            writer.queue(cursor::MoveTo(0, y.try_into().unwrap_or(0)))?;
            let mut prev_cells_unrendered = 0;

            for x in 0..buffer.size().x {
                let pos = Vec2::new(x, y);

                let cell = buffer.get(pos).unwrap();

                // Check if we can skip
                if buffers_size_eq {
                    let prev_cell = prev_buffer.get(pos).unwrap();
                    if cell == prev_cell {
                        prev_cells_unrendered += 1;
                        continue;
                    }
                }

                if prev_cells_unrendered != 0 {
                    writer.queue(cursor::MoveToColumn(
                        x.try_into().unwrap_or(0),
                    ))?;
                }

                prev_cells_unrendered = 0;

                // Get the style
                let fg = cell.style.fg.map_or(CrossColor::Reset, Into::into);
                let bg = cell.style.bg.map_or(CrossColor::Reset, Into::into);
                let underline_color = cell
                    .style
                    .underline_color
                    .map_or(CrossColor::Reset, Into::into);
                let mut modifiers = cell.style.add_modifier;
                modifiers.remove(cell.style.sub_modifier);
                let attrs: Vec<CrossAttribute> = modifiers.into();
                let attrs: CrossAttributes = (attrs.as_slice()).into();

                // And apply it
                writer
                    .queue(SetAttribute(CrossAttribute::Reset))?
                    .queue(SetForegroundColor(fg))?
                    .queue(SetBackgroundColor(bg))?
                    .queue(SetUnderlineColor(underline_color))?
                    .queue(SetAttributes(attrs))?;

                // Write the text
                writer.queue(Print(&cell.text))?;
            }
        }

        writer.flush()?;

        Ok(())
    }
}
