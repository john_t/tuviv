//! The themes of a border

use crate::{Modifier, Style};

use super::Border;

impl Border {
    /// A border looking like this:
    ///
    /// ```txt
    /// +---+---+
    /// |   |   |
    /// +---+---+
    /// |   |   |
    /// +---+---+
    /// ```
    pub const ASCII: Border = Border {
        up_left: Some('+'),
        up: Some('-'),
        up_right: Some('+'),
        right: Some('|'),
        down_right: Some('+'),
        down: Some('-'),
        down_left: Some('+'),
        left: Some('|'),

        up_down: Some('|'),
        left_right: Some('-'),

        up_down_left: Some('+'),
        up_down_right: Some('+'),
        left_right_down: Some('+'),
        left_right_up: Some('+'),

        left_right_up_down: Some('+'),

        style: def_style(),
    };

    /// A border looking like this:
    ///
    /// ```txt
    ///     +    
    ///     |    
    /// +---+---+
    ///     |    
    ///     +    
    /// ```
    pub const PSQL: Border = Border {
        up_left: None,
        up: None,
        up_right: None,
        right: None,
        down_right: None,
        down: None,
        down_left: None,
        left: None,

        up_down: Some('|'),
        left_right: Some('-'),

        up_down_left: Some('+'),
        up_down_right: Some('+'),
        left_right_down: Some('+'),
        left_right_up: Some('+'),

        left_right_up_down: Some('+'),

        style: def_style(),
    };

    /// A border looking like this:
    ///
    /// ```txt
    ///          
    /// |   |   |
    /// +---+---+
    /// |   |   |
    ///          
    /// ```
    pub const GITHUB_MARKDOWN: Border = Border {
        up_left: None,
        up: None,
        up_right: None,
        right: Some('|'),
        down_right: None,
        down: None,
        down_left: None,
        left: Some('|'),

        up_down: Some('|'),
        left_right: Some('-'),

        up_down_left: Some('|'),
        up_down_right: Some('|'),
        left_right_down: Some('+'),
        left_right_up: Some('+'),

        left_right_up_down: Some('+'),

        style: def_style(),
    };

    /// A border looking like this:
    ///
    /// ```txt
    /// ┌───┬───┐
    /// │   │   │
    /// ├───┼───┤
    /// │   │   │
    /// └───┴───┘
    /// ```
    pub const MODERN: Border = Border {
        up_left: Some('┌'),
        up: Some('─'),
        up_right: Some('┐'),
        right: Some('│'),
        down_right: Some('┘'),
        down: Some('─'),
        down_left: Some('└'),
        left: Some('│'),

        up_down: Some('│'),
        left_right: Some('─'),

        up_down_left: Some('┤'),
        up_down_right: Some('├'),
        left_right_down: Some('┬'),
        left_right_up: Some('┴'),

        left_right_up_down: Some('┼'),

        style: def_style(),
    };

    /// A border looking like this:
    ///
    /// ```txt
    /// ╭───┬───╮
    /// │   │   │
    /// ├───┼───┤
    /// │   │   │
    /// ╰───┴───╯
    /// ```
    pub const ROUNDED: Border = Border {
        up_left: Some('╭'),
        up: Some('─'),
        up_right: Some('╮'),
        right: Some('│'),
        down_right: Some('╯'),
        down: Some('─'),
        down_left: Some('╰'),
        left: Some('│'),

        up_down: Some('│'),
        left_right: Some('─'),

        up_down_left: Some('┤'),
        up_down_right: Some('├'),
        left_right_down: Some('┬'),
        left_right_up: Some('┴'),

        left_right_up_down: Some('┼'),

        style: def_style(),
    };

    /// A border looking like this:
    ///
    /// ```txt
    /// ╔═══╦═══╗
    /// ║   ║   ║
    /// ╠═══╬═══╣
    /// ║   ║   ║
    /// ╚═══╩═══╝
    /// ```
    pub const DOUBLED: Border = Border {
        up_left: Some('╔'),
        up: Some('═'),
        up_right: Some('╗'),
        right: Some('║'),
        down_right: Some('╝'),
        down: Some('═'),
        down_left: Some('╚'),
        left: Some('║'),

        up_down: Some('║'),
        left_right: Some('═'),

        up_down_left: Some('╣'),
        up_down_right: Some('╠'),
        left_right_down: Some('╦'),
        left_right_up: Some('╩'),

        left_right_up_down: Some('╬'),

        style: def_style(),
    };

    /// A border looking like this:
    ///
    /// ```txt
    /// .........
    /// :   :   :
    /// :...:...:
    /// :   :   :
    /// :.......:
    /// ```
    pub const DOTS: Border = Border {
        up_left: Some('.'),
        up: Some('.'),
        up_right: Some('.'),
        right: Some(':'),
        down_right: Some(':'),
        down: Some('.'),
        down_left: Some(':'),
        left: Some(':'),

        up_down: Some(':'),
        left_right: Some('.'),

        up_down_left: Some(':'),
        up_down_right: Some(':'),
        left_right_down: Some('.'),
        left_right_up: Some('.'),

        left_right_up_down: Some(':'),

        style: def_style(),
    };

    /// A border looking like this:
    ///
    /// ```txt
    ///          
    ///          
    ///          
    ///          
    ///          
    /// ```
    ///
    /// Its blank
    pub const BLANK: Border = Border {
        up_left: None,
        up: None,
        up_right: None,
        right: None,
        down_right: None,
        down: None,
        down_left: None,
        left: None,

        up_down: None,
        left_right: None,

        up_down_left: None,
        up_down_right: None,
        left_right_down: None,
        left_right_up: None,

        left_right_up_down: None,

        style: def_style(),
    };

    /// A border looking like this:
    ///
    /// ```txt
    /// ┌╌╌╌┬╌╌╌┐
    /// ┆   ┆   ┆
    /// ├╌╌╌┼╌╌╌┤
    /// ┆   ┆   ┆
    /// └╌╌╌┴╌╌╌┘
    /// ```
    pub const DASHED: Border = Border {
        up_left: Some('┌'),
        up: Some('╌'),
        up_right: Some('┐'),
        right: Some('┆'),
        down_right: Some('┘'),
        down: Some('╌'),
        down_left: Some('└'),
        left: Some('┆'),

        up_down: Some('┆'),
        left_right: Some('╌'),

        up_down_left: Some('┤'),
        up_down_right: Some('├'),
        left_right_down: Some('┬'),
        left_right_up: Some('┴'),

        left_right_up_down: Some('┼'),

        style: def_style(),
    };
}

/// The default style of border
///
/// This is only used because it is const nature
/// in defining the borders in this file
const fn def_style() -> Style {
    Style {
        fg: None,
        bg: None,
        underline_color: None,
        add_modifier: Modifier::NONE,
        sub_modifier: Modifier::NONE,
    }
}
