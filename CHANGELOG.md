# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased

### Changed

- The `Widget` trait to take a `BufferSliceMut<'_>`

## v0.7.0

### Added

- Coloured underlines
- Fancy underlines (undercurl, double underline, &c)
- `Style::realize_color` to realize colours for the use in
  stacks.
- Support for the `either` crate

### Removed

- Termion backend

### Fixed

- Certain Modifiers builder methods being incorrect

## v0.6.1

### Fixed

- KittyImage not resizing properly
- Cache storing one over the max_capacity

## v0.6.0

### Added

- A `max_capacity` to `Cache`, allowing for a maxmimum number
  of cache frames to be stored. Useful with:
- Kitty Images (requires feature `kitty_image`)
- The `mapping` field of LayoutRecord, allowing a function
  to be called before the layout is recorded.  For use
  in conjunction with scroll
- A function `WidgetExt::to_mapped_layout_record`, to
  create a layout record with the `mapping` field

### Changed

- Upgrade to Layout Engine `0.6.0`
- Use `Cell` not `RefCell` in focus position

### Fixed

- Centering issues on `Grid`
- Exported `scroll::FocusPosition`

## v0.5.0

### Added

- An `App` struct to make async tuviv easier
- A `Stack` widget to make stacking widgets easier
- A `Image` widget to render images
- A `Ratio` widget to render a child in a given ratio
- Serde support for `Style`
- A `Cache` widget to cache a widget
- Property `focus_position` to `Scroll`

### Changed

- `Scroll`'s rendering to always take up at least the
  available space.
- `ProgressBar` to be able to take multiple edges
- Upgrade to layout_engine 0.6.0
- Used generics in more places

### Fixed

- Progress bar panicking on a value of 0

## v0.4.0

### Added

- A `Char` widget (eda56e6)
- A `run_app` function to simplify making apps (677d679)
- A `run_frame` function to simplify examples (e44c131)
- `SizeConstraint` as a replacement to `FixedSize` It now takes ranges
  (e.g. width between 10 and 15, `10..15`) instead of `Option<usize>`, thus
  allowing more intricate constraints (8ab8715, 3a6c670)
- Vertical `ProgressBar`s (2e26379)
- A mouse example (96fbd3d)

### Changed

- Improved documentation (5f8e97b, 8158d8e, 4f411aa, 7407482, 1d243b7)
- Improved preformance (57c246c, fd48968, d74ce67, e494ed5, bc1fe09)

### Removed

- `FixedSize` (8ab8715)

### Fixed

- A bug with paragraphs not wrapping (e494ed5)
- Misplaced borders during rendering (2674a6a)
