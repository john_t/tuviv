//! Houses the [`Scroll`] widget

use std::{cell::Cell, ops::RangeInclusive, rc::Rc};

use le::{
    layout::{Rect, Vec2},
    Layout, MinimumNatural,
};

use crate::{buffer::BufferSliceMut, Buffer, Widget};

/// A region that must be in view of a scroll
#[derive(Clone, Debug, PartialEq, Eq)]
pub enum FocusPosition {
    /// A fixed rect
    Rect(Rect),
    /// An rc. Use with [`LayoutRecord`](super::LayoutRecord)
    Rc(Rc<Cell<Rect>>),
}

impl FocusPosition {
    /// Gets the rect of the [`FocusPosition`]
    pub fn get_rect(&self) -> Rect {
        match self {
            FocusPosition::Rect(rect) => *rect,
            FocusPosition::Rc(rc) => rc.get(),
        }
    }
}

impl From<Rect> for FocusPosition {
    fn from(value: Rect) -> Self {
        Self::Rect(value)
    }
}

impl From<Rc<Cell<Rect>>> for FocusPosition {
    fn from(value: Rc<Cell<Rect>>) -> Self {
        Self::Rc(value)
    }
}

/// A widget containing an internal buffer to allow a widget to
/// be clipped and scrolled to show.
pub struct Scroll<T: Widget> {
    /// The widget as the child of the scroll
    pub widget: T,
    /// The position of the scroll
    pub position: Vec2,
    /// A region that must be in view (e.g. a widget)
    pub focus_position: Option<FocusPosition>,
    /// Should the x direction be scrolled?
    pub scroll_x: bool,
    /// Should the y direction be scrolled?
    pub scroll_y: bool,
}

impl<T: Widget> Scroll<T> {
    /// Creates a new scroll scrolling in both x and y.
    ///
    /// See [`WidgetExt`](crate::prelude::WidgetExt::to_scroll) for
    /// a builder method
    pub fn new(widget: T) -> Self {
        Self {
            widget,
            position: Vec2::new(0, 0),
            focus_position: None,
            scroll_x: true,
            scroll_y: true,
        }
    }

    /// Sets the [`Scroll::position`] field
    pub fn position(mut self, position: Vec2) -> Self {
        self.position = position;
        self
    }

    /// Sets the [`Scroll::scroll_x`] field
    pub fn scroll_x(mut self, scroll_x: bool) -> Self {
        self.scroll_x = scroll_x;
        self
    }

    /// Sets the [`Scroll::scroll_y`] field
    pub fn scroll_y(mut self, scroll_y: bool) -> Self {
        self.scroll_y = scroll_y;
        self
    }

    /// Sets the [`Scroll::focus_position`] field
    pub fn focus_position(
        mut self,
        focus_position: impl Into<FocusPosition>,
    ) -> Self {
        self.focus_position = Some(focus_position.into());
        self
    }
}

impl<T: Widget> Layout for Scroll<T> {
    fn width_for_height(&self, height: usize) -> MinimumNatural<usize> {
        if self.scroll_x {
            MinimumNatural {
                minimum: 1,
                natural: self.widget.width_for_height(height).natural,
            }
        } else {
            self.widget.width_for_height(height)
        }
    }

    fn height_for_width(&self, width: usize) -> MinimumNatural<usize> {
        if self.scroll_y {
            MinimumNatural {
                minimum: 1,
                natural: self.widget.height_for_width(width).natural,
            }
        } else {
            self.widget.height_for_width(width)
        }
    }

    fn prefered_size(&self) -> MinimumNatural<Vec2> {
        match (self.scroll_x, self.scroll_y) {
            (true, true) => MinimumNatural {
                minimum: Vec2::new(1, 1),
                natural: self.widget.prefered_size().natural,
            },
            (true, false) => MinimumNatural {
                minimum: Vec2::new(1, self.widget.prefered_size().minimum.y),
                natural: self.widget.prefered_size().natural,
            },
            (false, true) => MinimumNatural {
                minimum: Vec2::new(self.widget.prefered_size().minimum.x, 1),
                natural: self.widget.prefered_size().natural,
            },
            (false, false) => self.widget.prefered_size(),
        }
    }

    fn prefered_size_of_container(
        &self,
        container: Vec2,
    ) -> MinimumNatural<Vec2> {
        match (self.scroll_x, self.scroll_y) {
            (true, true) => MinimumNatural {
                minimum: Vec2::new(1, 1),
                natural: self
                    .widget
                    .prefered_size_of_container(container)
                    .natural,
            },
            (true, false) => MinimumNatural {
                minimum: Vec2::new(
                    1,
                    self.widget.prefered_size_of_container(container).minimum.y,
                ),
                natural: self
                    .widget
                    .prefered_size_of_container(container)
                    .natural,
            },
            (false, true) => MinimumNatural {
                minimum: Vec2::new(
                    self.widget.prefered_size_of_container(container).minimum.x,
                    1,
                ),
                natural: self
                    .widget
                    .prefered_size_of_container(container)
                    .natural,
            },
            (false, false) => self.widget.prefered_size_of_container(container),
        }
    }
}

impl<T: Widget> Widget for Scroll<T> {
    fn render(&self, buffer: &mut BufferSliceMut<'_>) {
        let buffer_size = buffer.size();
        // Create a temporary buffer for the widget
        // to render upon.
        let mut widget_buffer = Buffer::new(Vec2::new(
            if self.scroll_x {
                let size = self.widget.width_for_height(buffer_size.y);
                if size.natural < buffer_size.x {
                    size.natural
                } else {
                    size.minimum.max(buffer_size.x)
                }
            } else {
                buffer_size.x
            },
            if self.scroll_y {
                let size = self.widget.height_for_width(buffer_size.x);
                if size.natural < buffer_size.y {
                    size.natural
                } else {
                    size.minimum.max(buffer_size.y)
                }
            } else {
                buffer_size.y
            },
        ));

        // Ensure that the focus_position is in display
        let mut region = Rect {
            start: self.position,
            size: buffer_size,
        };

        if self.scroll_x {
            if let Some(focus) = &self.focus_position {
                let focus = focus.get_rect();
                let adapted = adapt_range_to_range(
                    region.start.x..=region.end().x,
                    focus.start.x..=focus.end().x,
                );
                region.start.x = *adapted.start();
                region.size.x = adapted.end() - adapted.start();
            }
        } else {
            region.start.x = 0;
        }
        if self.scroll_y {
            if let Some(focus) = &self.focus_position {
                let focus = focus.get_rect();
                let adapted = adapt_range_to_range(
                    region.start.y..=region.end().y,
                    focus.start.y..=focus.end().y,
                );
                region.start.y = *adapted.start();
                region.size.y = adapted.end() - adapted.start();
            }
        } else {
            region.start.y = 0;
        }

        // Render the widget to the buffer
        self.widget.render(&mut widget_buffer.as_mut_slice());

        widget_buffer.crop(region);

        buffer.write_buffer(widget_buffer, Vec2::new(0, 0));
    }
}

/// Ensures that region is in focus
fn adapt_range_to_range(
    region: RangeInclusive<usize>,
    focus: RangeInclusive<usize>,
) -> RangeInclusive<usize> {
    let mut f0 = *region.start();
    let f1 = *region.end();
    let focus_size = focus.end() - focus.start();
    let region_size = region.end() - region.start();

    if *focus.start() < f0 {
        f0 = *focus.start();
    } else if *focus.end() > f1 {
        f0 = *focus.end() - region_size;
    }

    f0..=(f0 + focus_size)
}
