//! # Widgets
//!
//! Tuviv comes with many widgets by default and they can be
//! broadly catagorised into three catagories:
//!
//! ## Display Widgets
//!
//! These widgets directly render something on screen,
//! and do not contain children. These include:
//!
//!  * [`Paragraph`] - renders text
//!  * [`Filler`] - renders text on repeat
//!  * [`ProgressBar`] - renders a progress bar
//!  * [`Char`] - renders a [`prim@char`]
//!  * [`Image`] - displays an image in the terminal (not using kitty/sixel/&c)
//!                Requires the `image` dependency to be enabled.
//!  * [`KittyImage`] - displays an image in the terminal (using kitty)
//!                     Requires the `kitty` dependency to be enabled.
//!
//! ## Layout Containers
//!
//! These containers take multiple children and arrange them nicely:
//!
//!  * [`Flexbox`] - arranges items simmilar to CSS flexbox
//!  * [`Grid`] - arranges items simmilar to CSS grid
//!  * [`Stack`] - stacks widgets on top of each other
//!
//! ## Bin Containers
//!
//! These containers take one child. Please note that many of
//! these widgets can be constructed via builder methods in
//! [`WidgetExt`](crate::prelude::WidgetExt) for much cleaner
//! code.
//!
//!  * [`SizeConstraint`] - mandates a specific size or range of sizes for a child
//!  * [`Align`] - aligns a child, useful for centering
//!  * [`LayoutRecord`] - records the position of a widget
//!  * [`RefWidget`] - stores a child in multiple places
//!  * [`BoxSizing`] - give an item padding, borders and margins
//!  * [`Scroll`] - allows a widget to scroll
//!  * [`Ratio`] - forces a widget to a certain ratio
//!  * [`Cache`] - caches a widget's display at a certain height
//!
//! ## Other
//!
//! - [`either::Either`] - uses the `either` crate to render either the left
//!              or right widget

/// Flexbox related items
pub mod flexbox;
pub use flexbox::Flexbox;

mod size_constraint;
pub use size_constraint::SizeConstraint;

mod align;
pub use align::Align;

/// Grid related items
pub mod grid;
pub use grid::Grid;

mod char;
pub use self::char::Char;

mod layout_record;
pub use layout_record::LayoutRecord;

mod ref_widget;
pub use ref_widget::RefWidget;

mod paragraph;
pub use paragraph::Paragraph;

pub mod box_sizing;
pub use box_sizing::BoxSizing;

mod filler;
pub use filler::Filler;

mod scroll;
pub use scroll::*;

mod progress_bar;
pub use progress_bar::ProgressBar;

mod stack;
pub use stack::Stack;

#[cfg(feature = "either")]
mod either;

#[cfg(feature = "image")]
mod image;
#[cfg(feature = "image")]
pub use self::image::Image;

#[cfg(feature = "kitty")]
mod kitty_image;
#[cfg(feature = "kitty")]
pub use kitty_image::*;

mod ratio;
pub use ratio::Ratio;

pub mod cache;
pub use cache::Cache;
