//! Houses the [`BoxSizing`] widget and associated types

use le::{
    layout::{Rect, Vec2, TRBL},
    Layout, MinimumNatural,
};

use crate::Widget;
use crate::{border::Border, buffer::BufferSliceMut};

/// Adds padding, margins and a border to a child widget.
///
/// ```txt
/// + - - - - - - - - - - +
/// |   MARGIN            |
///   +-BORDER--------+
/// | | PADDING       |   |
///   | +-----------+ |
/// | | |   WIDGET  | |   |
///   | +-----------+ |
/// | +---------------+   |
/// + - - - - - - - - - - +
/// ```
pub struct BoxSizing<T: Widget> {
    /// The child widget
    pub widget: T,
    /// The border to put around it
    pub border: Border,
    /// The padding
    pub padding: TRBL,
    /// The margins
    pub margin: TRBL,
}

impl<T: Widget> BoxSizing<T> {
    /// Creates a new [`BoxSizing`] with no margins or padding.
    ///
    /// See [`WidgetExt`](crate::prelude::WidgetExt::to_box_sizing) for
    /// a builder method
    pub fn new(widget: T, border: Border) -> Self {
        Self {
            widget,
            border,
            padding: TRBL::default(),
            margin: TRBL::default(),
        }
    }

    /// How much more space than the widget does this use?
    pub fn extra(&self) -> TRBL {
        self.padding + self.margin + self.border.border()
    }

    /// Sets the [`BoxSizing::border`]
    pub fn border(mut self, border: Border) -> Self {
        self.border = border;
        self
    }

    /// Sets the [`BoxSizing::border`]
    pub fn margin(mut self, margin: TRBL) -> Self {
        self.margin = margin;
        self
    }

    /// Sets the left margin
    pub fn margin_left(mut self, margin: usize) -> Self {
        self.margin.left = margin;
        self
    }

    /// Sets the right margin
    pub fn margin_right(mut self, margin: usize) -> Self {
        self.margin.right = margin;
        self
    }

    /// Sets the top margin
    pub fn margin_top(mut self, margin: usize) -> Self {
        self.margin.top = margin;
        self
    }

    /// Sets the bottom margin
    pub fn margin_bottom(mut self, margin: usize) -> Self {
        self.margin.bottom = margin;
        self
    }

    /// Sets the padding
    pub fn padding(mut self, padding: TRBL) -> Self {
        self.padding = padding;
        self
    }

    /// Sets the left padding
    pub fn padding_left(mut self, padding: usize) -> Self {
        self.padding.left = padding;
        self
    }

    /// Sets the right padding
    pub fn padding_right(mut self, padding: usize) -> Self {
        self.padding.right = padding;
        self
    }

    /// Sets the top padding
    pub fn padding_top(mut self, padding: usize) -> Self {
        self.padding.top = padding;
        self
    }

    /// Sets the bottom padding
    pub fn padding_bottom(mut self, padding: usize) -> Self {
        self.padding.bottom = padding;
        self
    }
}

impl<T: Widget> Layout for BoxSizing<T> {
    fn width_for_height(&self, height: usize) -> MinimumNatural<usize> {
        let extra = self.extra();
        self.widget
            .width_for_height(height.saturating_sub(extra.vertical()))
            + MinimumNatural::same(extra.horizontal())
    }

    fn height_for_width(&self, width: usize) -> MinimumNatural<usize> {
        let extra = self.extra();
        self.widget
            .height_for_width(width.saturating_sub(extra.horizontal()))
            + MinimumNatural::same(extra.vertical())
    }

    fn prefered_size(&self) -> MinimumNatural<Vec2> {
        let extra = self.extra();
        MinimumNatural::same(extra.extra()) + self.widget.prefered_size()
    }

    fn prefered_size_of_container(
        &self,
        container: Vec2,
    ) -> MinimumNatural<Vec2> {
        self.widget.prefered_size_of_container(
            container.saturating_sub(self.extra().extra()),
        ) + MinimumNatural::same(self.extra().extra())
    }
}

impl<T: Widget> Widget for BoxSizing<T> {
    fn render(&self, buffer: &mut BufferSliceMut<'_>) {
        let extra = self.extra();

        let widget_rect = Rect {
            start: Vec2::new(extra.left, extra.top),
            size: buffer.size().saturating_sub(extra.extra()),
        };

        let border_rect = Rect {
            start: Vec2::new(self.margin.left, self.margin.top),
            size: buffer.size().saturating_sub(self.margin.extra()),
        };

        self.border
            .draw_border(&mut buffer.sub_mut_slice(border_rect).unwrap());
        self.widget
            .render(&mut buffer.sub_mut_slice(widget_rect).unwrap());
    }
}
