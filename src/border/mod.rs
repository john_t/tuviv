//! Borders and outlines

use le::layout::{Vec2, TRBL};

use crate::{buffer::BufferSliceMut, prelude::StylableExt, Style};
mod themes;

/// A border of an object
#[derive(Debug, Clone, Eq, PartialEq, Default)]
pub struct Border {
    /// A grid segment: `┌`
    pub up_left: Option<char>,
    /// A grid segment: `─`
    pub up: Option<char>,
    /// A grid segment: `┐`
    pub up_right: Option<char>,
    /// A grid segment: `│`
    pub right: Option<char>,
    /// A grid segment: `┘`
    pub down_right: Option<char>,
    /// A grid segment: `─`
    pub down: Option<char>,
    /// A grid segment: `└`
    pub down_left: Option<char>,
    /// A grid segment: `│`
    pub left: Option<char>,

    /// A grid segment: `│`
    pub up_down: Option<char>,
    /// A grid segment: `─`
    pub left_right: Option<char>,

    /// A grid segment: `┤`
    pub up_down_left: Option<char>,
    /// A grid segment: `├`
    pub up_down_right: Option<char>,
    /// A grid segment: `┬`
    pub left_right_down: Option<char>,
    /// A grid segment: `┴`
    pub left_right_up: Option<char>,

    /// A grid segment: `┼`
    pub left_right_up_down: Option<char>,

    /// The colour of this border
    pub style: Style,
}

impl Border {
    /// Removes all horizontal separators of the border
    pub fn without_horizontal_separators(self) -> Self {
        Border {
            up_left: self.up_left,
            up: self.up,
            up_right: self.up_right,
            right: self.right,
            down_right: self.down_right,
            down: self.down,
            down_left: self.down_left,
            left: self.left,

            up_down: self.up_down,
            left_right: None,

            up_down_left: self.left,
            up_down_right: self.right,
            left_right_down: self.left_right_down,
            left_right_up: self.left_right_up,

            left_right_up_down: self.up_down,

            style: self.style,
        }
    }

    /// Removes all vertical separators of the borer
    pub fn without_vertical_separators(self) -> Self {
        Border {
            up_left: self.up_left,
            up: self.up,
            up_right: self.up_right,
            right: self.right,
            down_right: self.down_right,
            down: self.down,
            down_left: self.down_left,
            left: self.left,

            up_down: None,
            left_right: self.left_right,

            up_down_left: self.up_down_left,
            up_down_right: self.up_down_right,
            left_right_down: self.down,
            left_right_up: self.up,

            left_right_up_down: self.left_right,

            style: self.style,
        }
    }

    /// Whether this border has a top-section
    ///
    /// Returns true if `up`, `up_left` or `up_right` is `Some`.
    pub fn has_top(&self) -> bool {
        self.up.is_some() || self.up_left.is_some() || self.up_right.is_some()
    }

    /// Whether this border has a bottom-section
    ///
    /// Returns true if `down`, `down_left` or `down_right` is `Some`.
    pub fn has_bottom(&self) -> bool {
        self.down.is_some()
            || self.down_left.is_some()
            || self.down_right.is_some()
    }

    /// Whether this border has a right-section
    ///
    /// Returns true if `right`, `up_right` or `down_right` is `Some`.
    pub fn has_right(&self) -> bool {
        self.right.is_some()
            || self.up_right.is_some()
            || self.down_right.is_some()
    }

    /// Whether this border has a left-section
    ///
    /// Returns true if `left`, `up_left` or `down_left` is `Some`.
    pub fn has_left(&self) -> bool {
        self.left.is_some()
            || self.up_left.is_some()
            || self.down_left.is_some()
    }

    /// Gets the [`TRBL`] representing the border
    pub fn border(&self) -> TRBL {
        TRBL::new(
            usize::from(self.has_top()),
            usize::from(self.has_right()),
            usize::from(self.has_bottom()),
            usize::from(self.has_left()),
        )
    }

    /// Returns this with a style
    pub fn styled(mut self, style: Style) -> Self {
        self.style = style;
        self
    }

    /// Draws a border to a buffer
    pub fn draw_border(&self, buffer: &mut BufferSliceMut<'_>) {
        // Draw the corners
        if let Some(up_left) = self.up_left {
            buffer.write(
                Vec2::new(0, 0),
                up_left.to_string().with_style(self.style),
            );
        }

        if let Some(up_right) = self.up_right {
            buffer.write(
                Vec2::new(buffer.size().x - 1, 0),
                up_right.to_string().with_style(self.style),
            );
        }

        if let Some(down_left) = self.down_left {
            buffer.write(
                Vec2::new(0, buffer.size().y - 1),
                down_left.to_string().with_style(self.style),
            );
        }

        if let Some(down_right) = self.down_right {
            buffer.write(
                buffer.size() - Vec2::new(1, 1),
                down_right.to_string().with_style(self.style),
            );
        }

        // Draw the edges

        // Top
        if let Some(up) = self.up {
            buffer.write(
                Vec2::new(1, 0),
                up.to_string()
                    .repeat(buffer.size().x.saturating_sub(2))
                    .with_style(self.style),
            );
        }

        // Bottom
        if let Some(down) = self.down {
            buffer.write(
                Vec2::new(1, buffer.size().y.saturating_sub(1)),
                down.to_string()
                    .repeat(buffer.size().x.saturating_sub(2))
                    .with_style(self.style),
            );
        }

        let start_y = 1;
        let end_y = buffer.size().y.saturating_sub(1);

        // Left
        if let Some(left) = self.left {
            for y in start_y..end_y {
                buffer.write(
                    Vec2::new(0, y),
                    left.to_string().with_style(self.style),
                );
            }
        }

        // Right
        if let Some(right) = self.right {
            for y in start_y..end_y {
                buffer.write(
                    Vec2::new(buffer.size().x.saturating_sub(1), y),
                    right.to_string().with_style(self.style),
                );
            }
        }
    }
}
